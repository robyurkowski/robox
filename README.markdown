##Robox##

Robox is a utility IRC bot designed to be easily extendible with a minimal API learning curve. His name pays homage to a wonderful (but somewhat useless) robot from [Gunnerkrigg Court](http://www.gunnerkrigg.com).

###Installing###

1. git clone git@github.com:robyurkowski/robox.git
2. Edit config/config.yml and insert your own values.
3. Edit config/users.yml and insert your own values.

###Running###

Robox is designed to run in the background, but if you do not redirect his output, he will not properly do so (useful for debugging!).

To run him:

    ./robox >& /dev/null
    
If you want to run him as a long-standing background job on a server, I heartily recommend `screen`. To use this, on the command line, type:

    screen
    ./robox >& /dev/null
    
Press `ctrl + a`, and then `d`. When you log back in to SSH, you can pick him back up with:

    screen -x
    
... on the command line.
    
To authorize yourself to issue commands, ensure there is an entry for you in `config.users.yml` and send him a query:

    !login <password>
    
###Plugins###

Extensive documentation can be found in the plugin class. There are quite a few substantial examples in the plugins folder, too.

###Future###

Robox will be moving his configuration files to an SQL database soon.

###Thanks###

... to Jason van Dyke, who has since disappeared, but who is responsible, largely, for the plugin and language implementations.