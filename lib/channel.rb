# Channel object
#  Dynamic values
#  - users = a hash of user objects with user names as the key
#  Stored values
#  - name  = the name of the channel
#  - owner = only set if the channel is registered through robox (instead of chanserv)
#  - flags = flags that can be set through plugins and such for various purposes
#  - modes = a list of the current modes on the channel
#  - bans  = a list of bans for the current channel
#  - topic = the current topic of the channel
#  - attributes = storage for various purposes (ie: extensions and plugins)

$language.push "@chan_config_error", "Couldn't open the configuration file. You should check your file permissions." 

class Channel < IRCClient

  attr_accessor :flags, :modes, :attributes, :topic
  attr_reader :name, :users, :owner, :bans, :log

  def initialize(channel)
    channel = channel.downcase
    channel = "\##{channel}" unless channel =~ /^\#/
    return true              if @@channels.has_key?(channel)

    @name       = channel
    @flags      = Array.new
    @modes      = Array.new
    @users      = Hash.new
    @owner      = String.new
    @bans       = Array.new
    @topic      = String.new
    @attributes = Hash.new

    dir = channel.sub("\#","")
    Dir.mkdir "channels"        unless File.directory?("channels")
    Dir.mkdir "channels/#{dir}" unless File.directory?("channels/#{dir}")
    @log = Log.new("channels/#{dir}/log.log")

    if ( File.exists?("channels/#{dir}/config.yml") )
      begin
        config = File.open(file) { |f| YAML.load(f) }
      rescue
        @log.debug $language.chan_config_error
        exit(1)
      end

      config['modes'].to_a.each { |m| self.set_mode(m) }
#      config['flags'].to_a.each { |f| self.chan_set_flag(f) }
#      config['bans'].to_a.each  { |b| self.chan_set_ban( b) }
      self.chan_set_topic config['topic'] if ((defined? config['topic']) and !(config['topic'].empty?))
      @owner = config['owner']            if ((defined? config['owner']) and !(config['owner'].empty?))
      @attributes = config['attributes']  if (defined? config['attributes'])
    end

    @@channels[channel] = self
  end

  #TODO: Channel registration methods, write channel info methods (IRCClent), flags and bans

  #
  # USERS
  #
  def in_channel?(nick)
    return true if @users.has_key?(nick)
    return false
  end

  def add_user(user,mode = nil)
    return false unless @@users.has_key?(user)
    return true  if @users.has_key?(user)
    @users[user] = Array.new
    return true if mode.nil? or mode.to_s.empty?
    @@chan_user_flags.each_key { |k| @users[user] << @@chan_user_flags[k] if mode.include?(k) }
    return true
  end

  def rem_user(user)
    return false unless @users.has_key?(user)
    @users.delete(user)
    return true
  end

  #
  # MODES
  #
  def set_mode(mode,user = nil)
    return false unless mode =~ /^([+-].).*$/
    mode = ( $1 or mode[0..1] )
    if user.nil?
      super(mode,@name)
    else
      return false unless @users.has_key?(user)
      super(mode,@name,user)
    end
    return true
  end

  def add_mode(mode,user = nil)
    return false unless mode.size == 1
    if user.nil?
      @modes << mode unless @modes.include?(mode)
    else
      return false unless @users.has_key?(user)
      @users[user] << mode unless @users[user].include?(mode)
    end
    return true
  end

  def rem_mode(mode,user = nil)
    return false unless mode.size == 1
    if user.nil?
      @modes.delete(mode)
    else
      return false unless @users.has_key?(user)
      @users[user].delete(mode)
    end
    return true
  end

  #
  # FLAGS
  #
  def add_flag(flag)
    begin
      return true if @flags.include?(flag[0].chr)
      @flags << flag[0].chr
      return true
    rescue; return false; end
  end

  def rem_flag(flag)
    begin
      return true unless @flags.include?(flag[0].chr)
      @flags.delete(flag[0].chr)
      return true
    rescue; return false; end
  end

  # ALIASES
  def set_topic(topic);          super(@name,topic);       end # TOPIC
  def send_message(message);     super(@name,message);     end # MESSAGE
  def send_notice(message);      super(@name,message);     end # NOTICE
  def kick(user,reason = nil);   super(user,@name,reason); end # KICK
  def invite(user);              super(user,@name);        end # INVITE
  def set_mode(mode,user = nil); super(mode,user,@name);   end # MODE
  alias :msg :send_message
  alias :notice :send_notice
  alias :w :send_notice

end
