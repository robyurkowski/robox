class User < IRCClient

  # Default flags (not hard coded by any means, feel free to change them)
  #   - A: Administrator flag, this user has access to administrative functions.
  #   - O: Operator flag, this user has access to the ircop functions.
  #   - r: Registered user, has access to the basic functions.
  # If you do decide to change them, you will need to edit the access settings in
  # extensions/corecommand.rb and also in extensions/plugin.rb

  attr_reader :nickname, :hostmask, :username, :log, :accountname, :login
  attr_accessor :flags, :access, :channels, :attributes
  
  # nick!username@hostmask.tld
  def initialize(user)
    return false unless user =~ /(.+?)!(.+?)@([A-Za-z0-9\-\.\~]*)/
    @nickname         = $1
    @username         = $2
    @hostmask         = $3
    @accountname      = ""
    @login            = false
    @flags            = Array.new
    @access           = 0
    @channels         = Hash.new
    @attributes       = Hash.new

    if !(defined? @@deflog)
      Dir.mkdir "users"      unless File.directory?("users")
      Dir.mkdir "users/logs" unless File.directory?("users/logs")
      @@deflog = Log.new("users/logs/users.common.log")
    end

    @log = @@deflog
    @log.debug $language.user_log_create_obj % [self,user]
    @log.info  $language.user_log_create % [@nickname,@username,@hostmask]

    @@users[@nickname] = self unless @@users.has_key?(@nickname) and \
                                    @@users[@nickname].hostmask == @hostmask
  end

  # USER TRACKING
  # Methods required to track users as they scurry about on the IRC (like little
  # ants or something).
  # TODO: Add modes, quits, kicks, bans, ect...

  # add_channel("#robox","~+@%&")
  # TODO: Check channel modes before adding channels to hash.
  def add_channel(channel,mode="")
    @log.info $language.user_log_chan_add % [@username,@hostmask,@nickname,channel,mode]
    @channels[channel] = mode
  end

  # del_channel("#robox")
  def del_channel(channel)
    @log.info $language.user_log_chan_del % [@username,@hostmask,@nickname,channel]
    @channels.delete(channel)
  end

  # nick_change("newnick")
  def nick_change(newnick)
    @log.info $language.user_log_nick % [@username,@hostmask,@nickname,newnick]
    oldnick           = @nickname
    @nickname         = newnick
    @@users[@nickname] = self # Overwrite any erroneous user data
    @@users.delete(oldnick)
  end

  # Add default settings to a user account.
  # TODO: Add stuff as we come across it.
  def user_register(password)
    raise $language.user_is_registered % @nickname if user_exists?(@nickname)
    @log.debug $language.user_log_register % [@username,@hostmask,@nickname]
    user_hash               = Hash.new
    user_hash['nickname']   = @nickname
    user_hash['password']   = password
    user_hash['access']     = @@user_default_access
    user_hash['flags']      = @@user_default_flags
    user_hash['attributes'] = @attributes
    user_hash['time']       = Time.now.to_i
    user_record(user_hash)
    self.log_in(password)
    return true
  end

  # Adds an alias to your account.
  # TODO: Error catching.
  def user_register_alias
    raise $language.user_is_registered % @nickname if user_exists?(@nickname)
    raise $language.user_not_logged                unless self.logged_in?
    @log.debug $language.user_log_register_alias % [@username,@hostmask,@nickname,@accountname]
    user_hash          = Hash.new
    user_hash['alias'] = @accountname
    user_hash['time']  = Time.now.to_i
    user_record(user_hash)
    return true
  end

  # Drops your account and all associated aliases
  # TODO: Allow anyone with admin flag to do this regardless of who
  #   owns the account.
  def user_drop
    raise $language.user_not_registered % @nickname    unless user_exists?(@nickname)
    raise $language.user_not_logged                    unless self.logged_in?
    raise $language.user_does_not_match % @accountname unless @accountname == @nickname
    @log.debug $language.user_log_drop % [@username,@hostmask,@accountname]
    user_delete(@accountname)
    self.log_out
    return true
  end

  # Drop an alias from your account.
  # TODO: Allow anyone with admin flag to do this regardless of who
  #   owns the account.
  def user_drop_alias(name = nil)
    name = @nickname                                if name.nil?
    raise $language.user_not_registered % @nickname unless (account = user_exists?(name))
    raise $language.user_not_logged                 unless self.logged_in?
    raise $language.user_not_yours % name           unless @accountname == account
    @log.debug $language.user_log_drop_alias % [@username,@hostmask,name,@accountname]
    user_delete(name)
    return true
  end

  # Log in proceedure
  # TODO: Find a better way to make instance vars from hash values
  def log_in(password)
    raise $language.user_not_registered % @nickname unless (account = user_exists?(@nickname))
    user = user_lookup(account,'password','flags','access','attributes')
    raise $language.user_pass_fail                  unless user['password'] == password
    @accountname = account
    @login       = true
    @flags       = user['flags']
    @access      = user['access']
    @attributes  = user['attributes']
    @log         = Log.new("users/logs/#{@accountname}.log")
    @log.info $language.user_log_login % [@username,@hostmask,@accountname,Time.now.to_s]
    return true
  end

  # Log out proceedure
  def log_out
    raise $language.user_not_logged unless self.logged_in?
    @log.info $language.user_log_logout % [@username,@hostmask,Time.now.to_s]
    @accountname = ""
    @login       = false
    @flags       = Array.new
    @access      = 0
    @log         = @@deflog
    return true
  end

  # IRC COMMANDS
  # Any IRC actions that can be performed on a user should be placed below.
  # TODO: Create global versions of some of these commands.
  #   - ex. @@users['Jason'].global_kick would kick 'Jason' from every channel he appears in.
  # TODO: Could implement some error handling here, but that should already be taken
  #       care of within IRCClient. However it might be a good idea...
  def send_message(message);      super(@nickname,message);        end # MESSAGE
  def send_notice(message);       super(@nickname,message);        end # NOTICE
  def kick(channel,reason = nil); super(@nickname,channel,reason); end # KICK
  def invite(channel);            super(@nickname,channel);        end # INVITE
  def set_mode(mode,channel);     super(mode,@nickname,channel);   end # MODE
  alias :msg :send_message
  alias :notice :send_notice
  alias :w :send_notice
  
  def access_write(value)    super(@nickname,value);     end
  def flag_write(value)      super(@nickname,value);     end
  def flag_write(key,value)  super(@nickname,key,value); end
  def user_record(user_hash) super(@nickname,user_hash); end

  # Attr reader for login status.
  def logged_in?; @login; end
  
  def in_channel?(chan);
    return true if @channels.has_key?(chan)
    return false
  end

end

class IRCClient

  # Writes a single access value to the user register.
  def write_user_access(account,value)
    return false unless (account = user_exists?(account))
    user_hash = { "access" => value.to_i }
    user_record(account,user_hash)
    return true
  end

  # Writes a single flags value to the user register.
  def write_user_flag(account,value)
    value = value.to_s[0].chr
    return false unless (account = user_exists?(account))
    user = user_lookup(account,'flags')
    return true  if user['flags'].to_a.include?(value)
    user['flags'] = (user['flags'].to_a << value)
    user_record(account,user)
    return true
  end

  # Writes a single attribute to the user register.
  def write_user_attribute(account,key,value)
    return false unless (account = user_exists?(account))
    user = user_lookup(account,'attributes')
    user['attributes'] = Hash.new unless user['attributes'].class == Hash
    user['attributes'][key] = value
    user_record(account,user)
    return true
  end

  # USER REGISTRATION
  # Anything relating to the registration of nicks (and 
  # recording of user information relating to those account) should go in the section below.
  #  ie: registered user names, passwords, access levels, blacklisted commands/channels

  # Loads the user registry.
  # TODO: Make this file configuralbe?
  def users_load
    file = @@user_register
    File.open(file, "w") unless File.exists?(file)
    users = Hash.new     unless (users = File.open(file) {|f| YAML.load(f)})
    return users
  end

  # Writes to the user registry.
  # TODO: Add backups.
  def users_write(users_hash)
    file = @@user_register
    raise $language.user_registry_failed unless \
    File.open(file, "w") {|f| YAML.dump(users_hash, f)}
    return true
  end

  # Adds a user to the registry
  # Arguments - user information hash
  # TODO: Add some conditionals maybe? This will just blindly replace anything that
  #   happens to be there at the moment.
  def user_record(account,user_hash)
    users = users_load
    users[account] = Hash.new unless users.has_key?(account)
    users[account].merge!(user_hash)
    users_write(users)
    return true
  end

  def user_delete(key)
    users = users_load
    users.delete(key)
    users.delete_if {|k,v| ( v.has_key?('alias') and v['alias'] == key ) }
    users_write(users)
    return true
  end

  # Checks for the existence of a user account.
  # Returns a user name if account exists.
  # Returns false if one does not.
  def user_exists?(nickname)
    users = users_load
    return false    unless users.has_key?(nickname)
    return nickname unless users[nickname].has_key?('alias')
    return users[nickname]['alias']
  end

  def user_lookup(key,*attribute)
    users = users_load
    return nil unless users.has_key?(key)
    return nil if attribute.empty?
    result = Hash.new
    attribute.to_a.each { |a| result[a] = (users[key].has_key?(a)) ? users[key][a] : nil }
    return result
  end

end

