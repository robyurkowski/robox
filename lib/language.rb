#
# LANGUAGE CLASS
#    The language object is used to hold customizable responses and signals. The purpose
#    of this class is to make it easy to alter the bot's responses; ie: translating them
#    to a different language or leaving customized logs.
#
#  To add something to the language object:
#    $language.add "variable_name", "associated_message"
#    $language.append "@variable_name2", "this %s is awesome!"
#    $language.push "@varname_3", 25
#
#  To use this, do something like:
# def error_me_baby()
#   begin
#     if ( self.cause_an_error(true) )
#       raise $language.error_name % error_param
#     end
#   rescue; send_message(user,$!); end
# end
#
# def user_was_kicked(name,channel)
#   $users[name].log.info $language.user_was_kicked % [ name, channel ]
# end
#
class Language

  def initialize

    @okay                 = "Okay!"
    @error                = "Something went wrong: %s"
  # SYSTEM / INITIAL
    @init_core_load_fail  = "Couldn't find a core file. Exiting."
    @init_main_obj_failed = "Failed to create main object."
    @init_ext_load        = "Loading core extension: %s"
    @init_ext_fail        = "Loading failed. Continuing ..."
    @init_connect_fail    = "Failed to connect to the IRC server, is your server defined \
                             correctly in the config file?"
#   @init_std_libs_error  = "The socket and/or yaml libraries couldn't be found on your \
#                            computer. They should be part of the standard libs, so you \
#                            might have messed things up somehow."
#   @init_raw_mix         = "Couldn't load the raw mixin."
    @init_sys_log_err     = "Couldn't open or create the system log. Exiting."
    @init_log_start       = "Initializing logs."
    @init_dump_config     = "Dumping values: %s"
    @init_config_err      = "Your config file appears to be corrupted, or has a missing value. \
                             We're going to be nice and generate a new one for you..."
    @init_config_load_err = "The Config file (%s) could not be loaded. Please ensure \
                             your permissions are correct."
    
  # PRIMARY CLASS
    # SIGNALS
    @signal_user      = "USER %s 8 * :%s"
    @signal_nick      = "NICK %s"
    @signal_join      = "JOIN %s"
    @signal_part      = "PART %s"
    @signal_quit      = "QUIT :%s"
    @signal_kick      = "KICK %s %s :%s"
    @signal_mode      = "MODE %s %s"
    @signal_pong      = "PONG :%s"
    @signal_topic     = "TOPIC %s :%s"
    @signal_invite    = "INVITE %s %s"
    @signal_action    = "PRIVMSG %s :\001ACTION %s\001"
    @signal_privmsg   = "PRIVMSG %s :%s"
    @signal_notice    = "NOTICE %s :%s"
    @signal_mode_self = "MODE %s %s"
    @signal_mode_chan = "MODE %s %s"
    @signal_mode_user = "MODE %s %s %s"
    @signal_ctcp_ver  = "NOTICE %s :\001VERSION %s\001"
    @signal_ctcp_ping = "NOTICE %s :\001PING %s\001"
    @signal_ctcp_time = "NOTICE %s :\001TIME %s\001"
    
    # ERROR BLOCK
    @irc_not_on_chan     = "You're not on that channel. (%s)"
    @irc_bad_chan_name   = "Channel name was improperly formatted."
    @irc_join_no_arg     = "Join method was passed with no channels. Ignoring join instructions."
    @irc_already_on_chan = "Not joining %s, since we're already on it."
    @irc_part_no_arg     = "Part method was passed with no channels. Ignoring part instructions."
    @irc_not_on_chan     = "Not parting %s, since we're not on it."
    @irc_action_nil      = "Recipient or message was nil. Not performing action. '%s', '%s'"
    @irc_message_nil     = "Recipient or message was nil. Not sending message. '%s', '%s'"
    @irc_notice_nil      = "Recipient or message was nil. Not sending notice. '%s', '%s'"
    @irc_nick_nil        = "Nick was empty or nil. Not changing nick to '%s'"
    @irc_topic_nil       = "No channel was specified. Not changing topic."
    @irc_mode_nil        = "Mode was empty or nil. Not changing mode. '%s', '%s', '%s'"
    @irc_raw_no_method   = "No method for RAW(%s): %s"
    # LOGGING BLOCK
    @irc_quit            = "Quitting: %s"
    @irc_invite          = "Inviting (%s) to %s"
    @irc_join            = "Joining channel: %s"
    @irc_joined_channels = "Joined %s channels. Current channels: %s"
    @irc_part            = "Parting channel: %s"
    @irc_parted_channels = "Parted %s channels. Current channels: %s"
    @irc_nick            = "Changing bot nick from '%s' to '%s'."
    @irc_topic           = "Changing the topic in %s to: %s"
    @irc_raw_method      = "Calling: raw_%s --> %s"
    @irc_command         = "COMMAND %s [ %s!%s ] :%s"
    @irc_kick            = "Kicking %s from %s: %s"

  # HANDLE SERVER INPUTS
    @irc_raw       = "RAW{%s} %s"
    @irc_privmsg   = "  -%s- <%s> %s"
    @irc_unhandled = "Unhandled server message: %s"
    @ctcp_version  = "VERSION request from: %s"
    @ctcp_ping     = "PING request from: %s"
    @ctcp_time     = "TIME request from: %s"
  
  # CORE COMMANDS
    @core_no_help           = "There is no help for %s."
    @core_no_such_command   = "There is no such command."
    @core_uptime            = "Uptime: %s days, %s hours, %s minutes, %s seconds."

  # LOG CLASS
    @log_timestamp            = "--- [ %s ] ---"
    @log_load_error           = "Couldn't open the log file! Check that your file \
                                permissions are correct.\nIn the meantime, we will \
                                attempt to recreate this log in your home directory."
    @log_toggle_error         = "Log.log_users requires boolean input -- true or false."

  # USERS CLASS
    # ERROR BLOCK
    @user_registry_failed = "Could not open the user registry for writing."
    @user_not_registered  = "%s is not a registered user!"
    @user_is_registered   = "%s is already a registered user!"
    @user_does_not_match  = "You must be under your primary username (%s) to do that!"
#   @user_no_object       = "User object does not exist for %s."
    @user_not_logged      = "You must be logged in to do that!"
#    @user_not_alias       = "%s is not an alias!"
    @user_not_yours       = "%s is not registered under your account!"
    @user_pass_fail       = "You entered an invalid password."
    @user_not_authorized  = "You don't have the permission to do that: %s"
    # EVENT BLOCK
    @channel_topic      = " ^^ %s (%s) SETS TOPIC IN %s TO %s"
    @user_joins_channel = " -> %s (%s) JOINS %s"
    @user_parts_channel = " <- %s (%s) PARTS %s"
    @user_is_kicked     = " <- %s (%s) KICKS %s FROM %s :%s"
    @user_changed_nick  = " -- %s (%s) SWITCHED NICK TO %s"
    @user_has_quit      = " <- %s (%s) QUIT (%s)"
    @irc_mode           = " -- %s (%s) SETS MODE %s ON %s IN %s"
    @irc_chan_mode      = " -- %s (%s) SETS MODE %s ON %s"
    @irc_self_mode      = " -- %s SETS MODE %s ON %s"
    # CORE COMMAND RESPONSES
    @user_login_success      = "You are now logged in."
    @user_logout_success     = "You've been logged out successfully."
    @user_alias_success      = "The alias '%s', has been registered to your account."
    @user_register_success   = "You have registered the account '%s', don't forget your password!"
    @user_drop_alias_success = "You have dropped the alias '%s' from your account."
    @user_drop_success       = "Your account has been dropped, the name '%s' is now free for registration."
    # LOG BLOCK
    @user_log_create_obj     = " --> Creating new user object :%s - %s"
    @user_log_create         = " --> Nickname:%s, Username:%s, Hostmask:%s"
    @user_log_chan_add       = "[ %s!%s ] Adding channel data for %s: %s %s"
    @user_log_chan_del       = "[ %s!%s ] Removing channel data for %s: %s"
    @user_log_nick           = "[ %s!%s ] Changing nickname for %s: %s"
    @user_log_register       = "[ %s!%s ] Registering new account under: %s"
    @user_log_drop           = "[ %s!%s ] Dropping account (%s) and all associated aliases."
    @user_log_register_alias = "[ %s!%s ] Registering '%s' as an alias for '%s'."
    @user_log_drop_alias     = "[ %s!%s ] Dropping alias '%s' from account (%s)."
    @user_log_login          = "[ %s!%s ] %s has logged in... %s"
    @user_log_logout         = "[ %s!%s ] Logging off... %s"

  # THIS MUST GO AT THE END OF INITIALIZATION
    self.instance_variables.each {|v| self.class.send :attr_reader, ( v[1..(v.size)].intern ) }

  end

  def add(var = nil,value = nil)
    begin
      raise "Variable format incorrect." unless var =~ /^(@)?[\w][\w\d_]*$/
      var = "@#{var}"                    unless defined? $1
      raise "Variable already defined."  if self.instance_variables.include? var
      raise "Nil value unacceptable."    if value.nil?
    rescue; return false; end
    self.instance_variable_set var, value
    self.class.send :attr_reader, ( var[1..(var.size)].intern )
    return true
  end
  alias :push   :add
  alias :append :add

  def alter(var = nil, value = nil)
    begin
      raise "Variable format incorrect."    unless var =~ /^(@)?[\w][\w\d_]*$/
      var = "@#{var}"                       unless defined? $1
      raise "That variable does not exist!" unless self.instance_variables.include? var
      raise "Nil value unacceptable."       if value.nil?
    rescue; return false; end
    self.instance_variable_set var, value
    return true
  end
  alias :mod :alter
  alias :set :alter

end

$language = Language.new
