# This library is essentially a replica of 413x's basic bot posted on 
# http://snippets.dzone.com/posts/show/1785
# Given that it's replicated there, and I didn't really see it elsewhere,
# I'm assuming it's in public domain. Thank you, 413x!

# TODO: Refactor all non-call methods that use 'puts' to return values instead.
# We want to set that optional, or want to pipe it to @@irc, too.
# What we should do is use the logger.

# The irc class, which talks to the server and holds the main event loop
class IRCClient

  attr_accessor :access_mode, :user_default_access, :user_register

  def users;    @@users;    end
  def channels; @@channels; end
  def state;    @@state;    end
  def log;      @log;       end
  def socket;   @@irc;      end
  def commands; @@commands; end

  def initialize(config_location)
    # TODO: Make these configurable
    @@access_mode         = "flags" # only accepts "access" or "flags"
    @@user_default_access = 25
    @@user_default_flags  = "r"
    @@user_register       = "config/users.yml"
    @@chan_user_flags     = { "~" => "P", "&" => "p", "@" => "o", "%" => "h", "+" => "v" }      

    @@users, @@channels, @@commands, @config = {}, {}, {}, {}
    @config = initialize_config(config_location)
    @@state = initialize_state(@config)
    @log    = initialize_log(@config['loggerlocation'])

    @log.debug $language.init_log_start
    @log.debug $language.init_dump_config % @config.inspect
  end

  # check_access(obj,hash)
  # obj => user object
  # hash => { "access" => 50, "flags" => "r" }
  # TODO: Switch mode based on flag/access settings ??
  def check_access(user = nil, req = nil)
    return false if user.nil? or req.nil?
    return false unless req.class == Hash
    if @@access_mode == "access"
      return false unless req.has_key?("access")
      return false unless user.respond_to?("access")
      return true  if user.access >= req["access"].to_i
    elsif @@access_mode == "flags"
      return false unless req.has_key?("flags")
      return false unless user.respond_to?("flags")
      req["flags"] = req["flags"].to_a.join.split("")
      req["flags"].each { |f| return false unless user.flags.include?(f) }
      return true
    else
      return false
    end
  end

  # Connect to the IRC server
  def connect()
    @@irc = TCPSocket.open(@config['server'], @config['port'])
    @log.debug @@irc
    return true
  end

  def disconnect(message = @@state['defaultquit'])
    swrite $language.signal_quit % message
    @log.debug $language.irc_quit % message
    $done = true
  end

  # handle_raw(integer/string,string)
  # sends a raw message to its corresponding method if it exists.
  def handle_raw(numeric, message)
    message.strip!
    run = "raw_#{numeric}(message)" 
    if self.private_methods.include?("raw_#{numeric}") or self.respond_to?("raw_#{numeric}")
      @log.debug $language.irc_raw_method % [numeric, message]
      eval(run)
    else
      @log.info $language.irc_raw_no_method % [numeric, message]
    end
  end

  #TODO: Capture proper exceptions.
  def initialize_config(config_location)
    begin
      config = open(config_location) {|f| YAML.load(f) }
      config.each do |c| 
        raise Error if c.empty?
        c.untaint
      end
    rescue
      if File.exists?(config_location)
        puts $language.init_config_load_err % config_location
        exit(1)
      else
        load 'utils/generate_config.rb'
        retry
      end
    end

    config.tap{|c| c.untaint }
  end

  def initialize_log(location)
    begin
      syslog  = "#{location}system/system.log"
      log = Log.new(syslog)
    rescue
      Kernel.system("touch #{syslog}")
      retry
    rescue
      puts $language.init_sys_log_err
      exit(1)
    end

    log
  end

  def initialize_state(config)
    state = config.clone
    state['channels'] = {}
    state['starttime'] = Time.now
    state['oldnickname'] = ""
    state
  end

  def invite(user, channel)
    @log.info $language.irc_invite % [user.to_a.join(', '), channel]
    if channel =~ /^#/
      if @@state['channels'].include?(channel)
        user.to_a.each {|u| swrite $language.signal_invite % [u, channel] }
        true
      else
        @log.debug $language.irc_not_on_chan % channel
        false
      end
    else
      @log.debug $language.irc_bad_chan_name
      false
    end
  end

  # Can either take a single argument: join_channel "#ipgt"
  # or an array of objects: join_channel ["#ipgt", "#test"]
  def join_channel(channel = nil)
    if channel.nil?
      @log.debug $language.irc_join_no_arg
      return false
    end

    channel.to_a.each do |c|
      if @@state['channels'].keys.include?(c)
        @log.debug $language.irc_already_on_chan % c
      else
        swrite $language.signal_join % c
        @log.info $language.irc_join % c
        @@state['channels'][c] = {} # Add this channel to our channels hash
        @@state['channels'][c]['users'] = [] # We'll store user info here
        # TODO: Is this going to bust because it's in local scope?
        (defined? joined) ? joined += 1 : joined = 1
      end
    end

    return false unless defined? joined
    @log.info $language.irc_joined_channels % [joined, @@state['channels'].keys.join(', ')]
    true
  end
  alias :join_channels :join_channel
  alias :join :join_channel  

  #TODO: Check for own modes and user modes to avoid getting server error messages.
  #TODO: Check that the user (and the bot) is even on the channel.
  #TODO: If no user object is found, and user is on the channel, skip mode checking for user.
  def kick(user, channel, reason = nil)
    @log.debug $language.irc_kick_nil % [user, channel] and return false if user.nil? or channel.nil?
    @log.debug $language.irc_not_on_chan % channel and return false unless @@state['channels'].has_key?(channel)

    swrite $language.signal_kick % [channel, user, reason]
    @log.info $language.irc_kick % [user, channel, reason]
    true
  end

  # Can either take a single argument: part_channel "#ipgt"
  # or an array of objects: part_channel ["#ipgt", "#test"]  
  def part_channel(channel = nil)
    if channel.nil?
      @log.debug $language.irc_part_no_arg
      return false
    end

    channel.to_a.each do |c|
      if !@@state['channels'].keys.include?(c)
        @log.debug $language.irc_not_on_chan % c
      else
        swrite $language.signal_part % c
        @log.info $language.irc_part % c 
        @@state['channels'].delete(c) # Remove this channel from our channels hash
        # TODO: Local variable out of scope here?
        (defined? parted) ? parted += 1 : parted = 1
      end
    end

    return false unless defined? parted
    @log.info $language.irc_parted_channels % [parted, @@state['channels'].keys.join(', ')]
    true
  end
  alias :part_channels :part_channel
  alias :part :part_channel

  def send_action(recipient = nil, message = nil)
    if recipient.nil? or message.nil?
      @log.debug $language.irc_action_nil % [recipient, message]
      return false
    end

    recipient.to_a.each {|r| swrite $language.signal_action % [r, message] }
    true
  end
  alias :action :send_action
  alias :act :send_action  

  def send_message(recipient = nil, message = nil)
    if recipient.nil? or message.nil?
      @log.debug $language.irc_message_nil % [recipient, message]
      return false
    end

    recipient.to_a.each {|r| swrite $language.signal_privmsg % [r, message] }
    true
  end
  alias :msg :send_message

  def send_nickname()
    swrite $language.signal_user % [@@state['nickname'],@@state['realname']]
    swrite $language.signal_nick % @@state['nickname']
    join_channel(@config['channels'])
    @log.debug "send_nickname() finished. "
    true
  end

  def send_notice(recipient = nil, message = nil)
    if recipient.nil? or message.nil?
      @log.debug $language.irc_notice_nil % [recipient, message]
      return false
    end

    recipient.to_a.each {|r| swrite $language.signal_notice % [r, message] }
    true
  end
  alias :notice :send_notice
  alias :w :send_notice

  # Various different styles:
  # set_mode("+iw") => MODE Robox +iw
  # set_mode("+c","#robox") => MODE #robox +c
  # set_mode("+o","#robox","Rob") => MODE #robox +o Rob
  # set_mode("+oo","#robox",["Rob","Jason"]) => MODE #robox +oo Rob Jason
  def set_mode(mode = nil, channel = nil, target = nil)
    if mode.nil?
      @log.debug $language.irc_mode_nil % [mode, target, channel]
      return false
    end

    if channel.nil? and target.nil?
      out = $language.signal_mode_self % [@@state['nickname'], mode]
    elsif target.nil?
      out = $language.signal_mode_chan % [channel, mode]
    else
      target = target.to_a * " "
      out = $language.signal_mode_user % [channel, mode, target]
    end
    @log.info out
    swrite out
    true
  end

  # set_nickname "Timmy"
  def set_nickname(nick = nil)
    if nick.nil? or nick.empty?
      @log.debug $language.irc_nick_nil % nick
      return false
    end

    swrite $language.signal_nick % nick
    # Keep the old nick in case we have an error.
    @@state['oldnickname'] = @@state['nickname']
    @@state['nickname'] = nick
    @log.info $language.irc_nick % [@@state['oldnickname'], nick]
    true
  end
  alias :nick :set_nickname  

  def set_topic(channel = nil, message = nil)
    if channel.nil?
      @log.debug $language.irc_topic_nil
      return false
    end

    swrite $language.signal_topic % [channel, message]
    @log.info $language.irc_topic % [channel, message]
    true
  end    

  # handle_message(string,hash)
  # - recipient *string => where a reply will be sent
  # - m *hash => { "nick" => ..., "mask" => ..., "channel" => ..., "message" => ..., "raw" => ... }
  # TODO: Add functionality.
  # Put core commands here (those not added through extensions or plugins).
  def handle_message(m)
    begin
      puts $language.irc_command % [m['channel'], m['nick'], m['mask'], m['message']]    
      command = m['message'].sub(@@state['responder'], "")
      @@commands.each do |key, c|
        if command =~ c.regex
          return false unless c.active
          unless check_access(@@users[m['nick']],c.access)
            send_notice m['nick'], $language.user_not_authorized % m['message']
            return true
          end
          c.block.call(self,m,c.regex.match(command))
          return true
        end
      end
      return false
    rescue
      send_message m['channel'], $!
    end
  end

  # HANDLE SERVER INPUTS
  # Takes a raw string from the server and decides where the information is to be sent.
  # Currently serving:
  # - Raw server messages (handle_raw).
  # - Channel/private messages, notices, and commands (handle_message).
  # - CTCP version, ping, and time.
  # - User quits, joins, parts, kicks, modes, nicks ($user,$channel methods).
  # - Server pings.
  # TODO: Handle server modes. 
  # TODO: Write 'set_mode' methods for users and channels.
  # TODO: Add additional message handlers as they are encountered.
  # TODO: IMPORTANT! Add flood handling here.
  def handle_server_input(s)
    case s.strip

    when /^:?[\w\.\-\s]*NOTICE AUTH :\*\*\* (.+?)$/
      case $1
      when /hostname/
        send_nickname()
      else
        $log.debug "Blowing bubbles, please standby."
        # Blow bubbles!
      end

    when /^:?[\w\.\-\s]*(\d{3})(.+?)$/ #RAW
      # puts $language.irc_raw % [$1,$2] # Lets hide these, they're ugly.
      handle_raw($1,$2)

    when /^:?(.+?)!(.+?) (PRIVMSG|NOTICE) (.+?) :?(.+?)$/i #USER MESSAGE
      m = {"nick" => $1, "mask" => $2, "channel" => $4, "message" => $5, "raw" => s}
      m['channel'] = ( m['channel'] == @@state['nickname'] ) ? m['nick'] : m['channel']

      User.new("#{m['nick']}!#{m['mask']}") unless @@users.has_key?(m['nick'])
      Channel.new(m['channel']) unless @@channels.has_key?(m['channel']) or m['channel'] =~ /^[^\#]/

      case m['message']
      when /^#{@@state['responder']}/ # USER COMMAND
        @@channels[m['channel']].log.puts $language.irc_privmsg % [m['channel'],m['nick'],m['message']] if @@channels.has_key?(m['channel'])
        handle_message m

      when /^[\001]VERSION[\001]/i # CTCP VERSION REQUEST
        puts $language.ctcp_version % m['mask']
        swrite $language.signal_ctcp_ver % [m['nick'],@@state['version']]

      when /^[\001]PING ?(\d+?)?[\001]/i # CTCP PING REQUEST
        puts $language.ctcp_ping % m['mask']
        timestamp = ($1.nil?) ? Time.now.to_i : $1
        swrite $language.signal_ctcp_ping % [m['nick'],timestamp]

      when /^[\001]TIME[\001]/i # CTCP TIME REQUEST
        puts $language.ctcp_time % m['mask']
        swrite $language.signal_ctcp_time % [m['nick'],Time.now.to_s]

      else
        @@channels[m['channel']].log.puts $language.irc_privmsg % [m['channel'],m['nick'],m['message']] if @@channels.has_key?(m['channel'])
        puts $language.irc_privmsg % [m['channel'],m['nick'],m['message']]

      end

    when /^:?((.+?)!.+?) JOIN :?(.+?)$/i # JOINING CHANNEL
      User.new($1)    unless @@users.has_key?($2)
      Channel.new($3) unless @@channels.has_key?($3)
      @@users[$2].add_channel $3
      @@users[$2].log.info $language.user_joins_channel % [$2,$1,$3]
      @@channels[$3].add_user $1
      @@channels[$3].log.puts $language.user_joins_channel % [$2,$1,$3]
      puts $language.user_joins_channel % [$2,$1,$3]

    when /^:?((.+?)!.+?) PART ([^\s]*) ?:?.*$/i # LEAVING CHANNEL
      @@users[$2].del_channel $3 and \
      @@users[$2].log.info $language.user_parts_channel % [$2,$1,$3] if @@users.has_key?($2)
      @@channels[$3].rem_user $2
      @@channels[$3].log.puts $language.user_parts_channel % [$2,$1,$3]
      puts $language.user_parts_channel % [$2,$1,$3]

    when /^:?((.+?)!.+?) KICK (.+?) ([^\s]*) ?:?(.*)$/i # KICK FROM CHANNEL
      @@users[$4].del_channel $3 and \
      @@users[$2].log.info $language.user_is_kicked % [$2,$1,$4,$3,$5] if @@users.has_key?($4)
      @@channels[$3].rem_user $4
      @@channels[$3].log.puts $language.user_is_kicked % [$2,$1,$4,$3,$5]
      puts $language.user_is_kicked % [$2,$1,$4,$3,$5]

    when /^:?((.+?)!.+?) NICK :?(.+?)$/i # NICK CHANGE
      @@users[$2].nick_change $3 and \
      @@users[$2].log.info $language.user_changed_nick % [$2,$1,$3] if @@users.has_key?($2)
      @@channels.each_key do |c|
        @@channels[c].users << { $3 => @@channels[c].users[$2] } if @@channels[c].users.has_key?($2)
        @@channels[c].rem_user $2
        @@channels[c].log.puts $language.user_changed_nick % [$2,$1,$3]
      end
      puts $language.user_changed_nick % [$2,$1,$3]

    when /^:?((.+?)!.+?) QUIT :?(.+?)$/i # QUIT SERVER
      if @@users.has_key?($2)
        @@users[$2].log_out if @@users[$2].logged_in?
        @@users[$2].channels = Hash.new
        @@users[$2].log.info $language.user_has_quit % [$2,$1,$3]
      end
      @@channels.each_key do |c|
        @@channels[c].rem_user $2
        @@channels[c].log.puts $language.user_has_quit % [$2,$1,$3]
      end
      puts $language.user_has_quit % [$2,$1,$3]

    when /^:?((.+?)!.+?) TOPIC ([^\s]*) ?:?(.*)$/i # CHANGE CHANNEL TOPIC
      @@channels[$3].topic = $4
      @@channels[$3].log.puts $language.channel_topic % [$2,$1,$3,$4]
      puts $language.channel_topic % [$2,$1,$3,$4]

    when /^:?((.+?)!.+?) MODE (.+?) (.+?)$/i # MODES
      if ( $4.split.size > 1 )
        Channel.new($3) unless @@channels.has_key?($3)
        target = $4.split; mode = target.shift
        puts $language.irc_mode % [$2,$1,mode,target.join(', '),$3]
        if mode[0].chr == "+"
          target.to_a.size.times { |x| @@channels[$3].add_mode mode[x+1].chr, target.to_a[x] }
        else; target.to_a.size.times { |x| @@channels[$3].rem_mode mode[x+1].chr, target.to_a[x] }
        end
      elsif ( $1 == @@state['nickname'] )
        puts $language.irc_self_mode % [$1,$4,$3]
      else
        Channel.new($3) unless @@channels.has_key?($3)
        puts $language.irc_chan_mode % [$2,$1,$4,$3]
        mode = $4
        if mode[0].chr == "+"
          (mode.size - 1).times { |x| @@channels[$3].add_mode mode[x+1] }
        else; (mode.size - 1).times { |x| @@channels[$3].rem_mode mode[x+1] }
        end
      end

    when /^:?PING :?(.+?)$/ # SERVER PING
      swrite $language.signal_pong % $1

    else
      puts $language.irc_unhandled % s # TOPIC, INVITE
      @log.debug $language.irc_unhandled % s
    end
  end

  def swrite(s)
    #TODO: Put some error handling here
    if s.size < 449
      @@irc.puts s
      sleep 1
    else
      server_cmd = s.slice(/^^([\S\s]).+?:/)
      s = s.sub(server_cmd,"")
      while s.size > 0
        s_part = s[0..(449 - server_cmd.size)]
        s.sub!(s_part,"")
        @@irc.puts "#{server_cmd}#{s_part}"
        sleep 1
      end
    end
  end

  # Just keep on trckin' until we disconnect
  # Check to see if there's any input from the socket @@irc
  # or from the $stdin. Wait forever.
  # TODO: We might also want to set it optional to use $STDIN
  def main_loop()
    $done = false
    while !($done)
      ready = select([@@irc, $stdin], nil, nil, nil)
      next if !ready
      for s in ready[0]
        if s == $stdin then
          return if $stdin.eof
          s = $stdin.gets.chomp
          if s =~ /^\//
            begin; puts eval($').to_s
            rescue; puts "Error: #{$!}"; end
          else
            swrite s
          end
        elsif s == @@irc then
          return if @@irc.eof
          s = @@irc.gets
          # puts s
          handle_server_input(s)
        end   
      end
    end
    disconnect
    @@irc.close
  end

  private :handle_server_input
  private :handle_message
  private :handle_raw
  private :initialize_config
  private :initialize_log
  private :initialize_state

end
