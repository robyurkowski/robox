class IRCClient
  # TODO Develop a way to respond to the person/command who triggered
  # these errors.

  # 311 WHOIS_FULL_ADDRESS ("<nick> <username> <address> * :<info>")
  # Creates a new user if it does not yet exist.
  # Botox Jason Jason The.Web.Guru * :Jason
  def raw_311(message)
    message =~ /^.+? (.+?) (.+?) (.+?) \* :?(.+?)$/
    User.new("#{$1}!#{$2}@#{$3}") unless @@users.has_key?($1)
  end
  private :raw_311

  # 319 WHOIS_ALL_CHANNELS ("<nick> :<channels>")
  # Adds channel info to a user object
  # Botox Jason :@#robox @#chat @#Godspawn #twist
  def raw_319(message)
    message =~ /^.+? (.+?) :?(.+?)$/
    nick = $1
    channels = $2.split
    if @@users.has_key?(nick)
      channels.each {|c|
        mode = c.scan(/[~+@&%]/).join
        c = c.gsub(/[~+@&%]/,"")
        @@users[nick].add_channel c, mode
      }
    end
  end
  private :raw_319

  # 352 WHO
  # target channel username address server nick flags :hops info
  #  Botox #robox Botox fhn-C973D220.hsd1.in.comcast.net irc.forkhammer.net Botox H :0 Robox Butterfat
  #  Botox #robox Robox fhn-635A1E86.reverse.accesscomm.ca irc.forkhammer.net Robox H :0 Robox Butterfat
  #  Botox #robox Jason The.Web.Guru irc.forkhammer.net Jason Hr@ :0 Jason
  def raw_352(message)
    # 1:channel, 2:username, 3:address, 4:nick, 5:flags 
    message =~ /^.+? (.+?) (.+?) (.+?) .+? (.+?) (.+?) :?.+?$/
    User.new("#{$4}!#{$2}@#{$3}") unless @@users.has_key?($4)
    @@users[$4].add_channel($1,$5)
    @@channels[$1].add_user($4,$5)
  end
  private :raw_352

  # 332 TOPIC
  # target channel topic
  #  Botox #robox :topic
  def raw_332(message)
    message =~ /^.+? (.+?) :?(.+?)$/
    Channel.new($1) unless @@channels.has_key?($1)
    @@channels[$1].topic = $2
  end
  private :raw_332

  # 366 END /NAMES LIST
  # After joining a channel, send a /WHO request to the server
  def raw_366(message)
    message =~ /^.+? (.+?) :?.*$/
    Channel.new($1) unless @@channels.has_key?($1)
    swrite "WHO :#{$1}"
  end
  private :raw_366

  # We shouldn't get this error either, since we should trap for errors
  # in respective commands.
  # 401 ERR_NOSUCHNICK ("<nickname> :No such nick/channel")  
  def raw_401(message)
    # For now, log it.
    @log.debug message
  end
  private :raw_401
  
  # 402 ERR_NOSUCHSERVER ("<server name> :No such server")
  def raw_402(message)
    # Log it. We'd get this error on networks where there's more than one server
    # so it's not an immediate priority. 
    @log.debug message
  end
  private :raw_402
  
  # 403 ERR_NOSUCHCHANNEL ("<channel name> :No such channel")
  def raw_403(message)
    # Log it.
    @log.debug message
  end
  private :raw_403

  # 404 ERR_CANNOTSENDTOCHAN ("<channel name> :Cannot send to channel")
  def raw_404(message)
    # We're not on the channel we're sending, so we shouldn't really get this
    # error in the first place — we ought to be trapping for this in
    # send_message as it is...
    @log.warn message
  end
  private :raw_404

  # 405 ERR_TOOMANYCHANNELS ("<channel name> :You have joined too many channels")
  def raw_405(message)
    a = message.split(" ")
    # If we get the error, we want to make sure our array reflects that we
    # haven't joined this channel. 
    if @@state['channels'].keys.include?(a[0])
      @@state['channels'].delete(a[0])
    end
    @log.debug message
  end
  private :raw_405

  # 406 ERR_WASNOSUCHNICK ("<nickname> :There was no such nickname")
  def raw_406(message)
    # Unlikely we'll ever get this message, since the bot shouldn't be
    # executing /whowas. Still, let's log it; plugins might use it later.
    @log.debug message
  end
  private :raw_406

  # 407 ERR_TOOMANYTARGETS ("<target> :Duplicate recipients. No message delivered")
  def raw_407(message)
    # Log this.
    @log.debug message
  end
  private :raw_407

  # 409 ERR_NOORIGIN (":No origin specified") *(for ping/pong)
  def raw_409(message)
    # This is an error we should never see, since ping responses are automated.
    @log.warn message
  end
  private :raw_409

  # 411 ERR_NORECIPIENT (":No recipient given (<command>)")
  def raw_411(message)
    # We shouldn't get this error, since we should trap for errors
    # in respective commands.
    @log.warn message
  end
  private :raw_411

  # 412 ERR_NOTEXTTOSEND (":No text to send")
  def raw_412(message)
    # We shouldn't get this error either, since we should trap for errors
    # in respective commands.    
    @log.warn message
  end
  private :raw_412

  # 413 ERR_NOTOPLEVEL ("<mask> :No toplevel domain specified")
  def raw_413(message)
    @log.debug message
  end
  private :raw_413

  # 414 ERR_WILDTOPLEVEL ("<mask> :Wildcard in toplevel domain")
  def raw_414(message)
    @log.debug message
  end
  private :raw_414

  # 421 ERR_UNKNOWNCOMMAND ("<command> :Unknown command")
  def raw_421(message)
    # This error we could easily get by making a typo in 'send'.
    @log.warn message
  end
  private :raw_421

  # 422 ERR_NOMOTD (":MOTD File is missing")
  def raw_422(message)
    # Who even cares ... ?
    @log.debug message
  end
  private :raw_422

  # 423 ERR_NOADMININFO ("<server> :No administrative info available")
  def raw_423(message)
    # Doubtfully important.
    @log.debug message
  end
  private :raw_423

  # 424 ERR_FILEERROR (":File error doing <file op> on <file>")
  def raw_424(message)
    # This is a server error, and we shouldn't really concern ourselves with it.
    # We'll elevate it to a warning just because it could affect a high-importance
    # command.
    @log.warn message
  end
  private :raw_424

  # 431 ERR_NONICKNAMEGIVEN (":No nickname given")
  def raw_431(message)
    @log.debug message
  end
  private :raw_431

  # 432 ERR_ERRONEUSNICKNAME ("<nick> :Erroneus nickname")
  def raw_432(message)
    # Log it.
    @log.debug message
  end
  private :raw_432

  # 433 ERR_NICKNAMEINUSE ("<nick> :Nickname is already in use")
  def raw_433(message)
    # It's conceivable that we could have gotten this error trying to change our
    # name. This is why we keep @@state['oldnick'] present...
    
    # TODO Maybe implement @config['altnickname'] in case our nickname is taken
    # on join.
    a = message.split(" ")
    if @@state['nickname'] = a[0]
      # Since we've got the error, we've not actually changed our nick.
      # We just need to restore @@state['nickname'] to what it was.
      @@state['nickname'] = @@state['oldnickname']
      @@state['oldnickname'] = ""
    end
    @log.debug message
  end
  private :raw_433

  # 436 ERR_NICKCOLLISION ("<nick> :Nickname collision KILL")
  def raw_436(message)
    # This is an error that shouldn't happen often these days, but regardless,
    # we'll put a warn.
    # TODO If this causes a disconnect, we need to handle this fatally.
    @log.warn message
  end
  private :raw_436

  # 441 ERR_USERNOTINCHANNEL ("<nick> <channel> :They aren't on that channel")
  def raw_441(message)
    a = message.split(" ")
    # Check to see if we have that nick on our list for that channel.
    # @@state['channels'] = {"#chat" => {"users" => ["User1"], "#cow" => {}}
    # a[0] = <nick>
    # a[1] = <channel>
    if @@state['channels'][a[1]]['users'].include?(a[0])
      @@state['channels'][a[1]]['users'].delete(a[0])
    end
    @log.debug message
  end
  private :raw_441

  # 442 ERR_NOTONCHANNEL ("<channel> :You're not on that channel")
  def raw_442(message)
    a = message.split(" ")
    # If we get the error, we want to make sure our array reflects that we
    # haven't joined this channel. 
    if @@state['channels'].keys.include?(a[0])
      @@state['channels'].delete(a[0])
    end
    @log.debug message
  end
  private :raw_442

  # 443 ERR_USERONCHANNEL ("<user> <channel> :is already on channel")
  def raw_443(message)
    a = message.split(" ")
    # Check to see if we have that nick on our list for that channel.
    # @@state['channels'] = {"#chat" => {"users" => ["User1"], "#cow" => {}}
    # a[0] = <user>
    # a[1] = <channel>
    if !(@@state['channels'][a[1]]['users'].include?(a[0]))
      # Here we insert the nick into the list...
      @@state['channels'][a[1]]['users'].push(a[0])
    end
    @log.debug message
  end
  private :raw_443

  # 444 ERR_NOLOGIN ("<user> :User not logged in")
  def raw_444(message)
    @log.debug message
  end
  private :raw_444

  # 445 ERR_SUMMONDISABLED (":SUMMON has been disabled")
  def raw_445(message)
    @log.debug message
  end
  private :raw_445

  # 446 ERR_USERSDISABLED (":USERS has been disabled")
  def raw_446(message)
    @log.debug message
  end
  private :raw_446

  # 451 ERR_NOTREGISTERED (":You have not registered")
  def raw_451(message)
    @log.debug message
  end
  private :raw_451

  # 461 ERR_NEEDMOREPARAMS ("<command> :Not enough parameters")
  def raw_461(message)
    @log.debug message
  end
  private :raw_461

  # 462 ERR_ALREADYREGISTRED (":You may not reregister")
  def raw_462(message)
    @log.debug message
  end
  private :raw_462

  # 463 ERR_NOPERMFORHOST (":Your host isn't among the privileged")
  def raw_463(message)
    @log.debug message
  end
  private :raw_463

  # 464 ERR_PASSWDMISMATCH (":Password incorrect")
  def raw_464(message)
    @log.debug message
  end
  private :raw_464

  # 465 ERR_YOUREBANNEDCREEP (":You are banned from this server")
  def raw_465(message)
    @log.fatal message
    puts "#{message}. This program will exit, since there's not much point in 
          trying to connect."
    exit(0)
  end
  private :raw_465

  # 467 ERR_KEYSET ("<channel> :Channel key already set")
  def raw_467(message)
    @log.debug message
  end
  private :raw_467

  # 471 ERR_CHANNELISFULL ("<channel> :Cannot join channel (+l)")
  def raw_471(message)
    @log.debug message
  end
  private :raw_471

  # 472 ERR_UNKNOWNMODE ("<char> :is unknown mode char to me")
  def raw_472(message)
    @log.debug message
  end
  private :raw_472

  # 473 ERR_INVITEONLYCHAN ("<channel> :Cannot join channel (+i)")
  def raw_473(message)
    @log.debug message
  end
  private :raw_473

  # 474 ERR_BANNEDFROMCHAN ("<channel> :Cannot join channel (+b)")
  def raw_474(message)
    @log.debug message
  end
  private :raw_474

  # 475 ERR_BADCHANNELKEY ("<channel> :Cannot join channel (+k)")
  def raw_475(message)
    @log.debug message
  end
  private :raw_475

  # 481 ERR_NOPRIVILEGES (":Permission Denied- You're not an IRC operator")
  def raw_481(message)
    @log.debug message
  end
  private :raw_481

  # 482 ERR_CHANOPRIVSNEEDED ("<channel> :You're not channel operator")
  def raw_482(message)
    @log.debug message
  end
  private :raw_482

  # 483 ERR_CANTKILLSERVER (":You cant kill a server!")
  def raw_483(message)
    @log.debug message
  end
  private :raw_483

  # 491 ERR_NOOPERHOST (":No O-lines for your host")
  def raw_491(message)
    @log.debug message
  end
  private :raw_491

  # 501 ERR_UMODEUNKNOWNFLAG (":Unknown MODE flag")
  def raw_501(message)
    @log.debug message
  end
  private :raw_501

  # 502 ERR_USERSDONTMATCH (":Cant change mode for other users")
  def raw_502(message)
    @log.debug message
  end
  private :raw_502
end
