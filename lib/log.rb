# This is a very simple logger. It doesn't do much but log.
# It took me a while to figure out why it wasn't working, then I realized the problem
# after I'd had the logger active for a while and I restarted the bot.

# TODO: Have the ability to disable individual logs.
#   ie: ~set user.logging false #=> would turn off user logging.
#   Because, frankly, after the debugging is finished, who cares
#   about recording every user object you create XD.

# Mostly got this. Now, you can check if you want to log by invoking
# Log.log_users?. I'm not actually squelching the output here, but
# that's something we could do if we want. I figure it's enough to
# read from config 'log users'? and call Log.log_users = (true|false)
# and deal with that in the application code. Do you think it's the
# role of the class to squelch?

require 'fileutils'

class Log
  @@log_users = true;
  # I added in a timestamp when logging begins, because I usually am never sure
  # if I should read up or down when looking at logs. Also, I made @file hold the
  # filename of the log instead of a reference to the open file. If the file is
  # always open, the log won't update until the program exits. ~Jason
  
  # Fortunately, Ruby buffers by default, so this isn't as terrible as it might
  # seem, speed-wise. ~Rob
  def initialize(filename)
    @file = filename
    @time = Time.now
		begin
      # Ensure the file can be opened
      temp = File.open(@file, 'a')
    rescue LoadError => detail
      puts $language.log_load_error
      puts detail.message()
      exit(0)
    end
    self.log $language.log_timestamp % Time.now.to_s
  end 
  
  def should_be_rotated?
    @time.day != Time.now.day
  end

  def Log.log_users?
    @@log_users
  end
  
  def Log.log_users=(state = false) 
    begin
      raise ArgumentError if (state != true && state != false) 
    rescue ArgumentError => detail
      puts $language.log_toggle_error
      puts detail.message()
    else
      @@log_users = state
    end
  end 

  # Before, the log file would only update when the program exited.
  # This way, the file updates in realtime, also we won't have a
  # large amount of open files at any given time. ~Jason
  def puts(s)
    if self.should_be_rotated?
      @time = Time.now
      begin
        parts = @file.split(/\//)
        path = File.join(*parts[0..-2])
        FileUtils.mv(@file, File.join(path, "#{@time.day}-#{@time.month}-#{@time.year}-#{parts.last}"))
        File.open(@file, 'a') do |f|
          f.puts $language.log_timestamp % Time.now.to_s
          f.puts s
        end
      rescue
        File.open(@file, 'a') {|f| f.puts "ERROR: An error occurred when rotating logs. Continuing to write to old log.\n#{s}" }
      end
    else
      File.open(@file, 'a') {|f| f.puts s }
    end
  end
  alias :log :puts

  # These are basically aliases of puts that add a little bit extra.
  def debug(s); self.puts "|D| #{s}"; end
  def  warn(s); self.puts "|W| #{s}"; end
  def fatal(s); self.puts "|F| #{s}"; end
  def  info(s); self.puts "|I| #{s}"; end

end
