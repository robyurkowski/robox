class Quotes < Plugin

  def initialize
    begin
    super

    @quotes_store = "plugins/enabled/quotes/quotes.yml"
    @quotes_bakup = "plugins/enabled/quotes/quotes.bak"

    if !File.exist?( @quotes_store )
      @log.debug "The quotes file does not exist, creating it."
      File.open( @quotes_store, "w" )
    end

    if !( @quotes = File.open( @quotes_store ) { |f| YAML.load(f) } )
      @log.debug "Could not find any quotes data, creating an empty array."
      @quotes = Hash.new
      @quotes['data'] = Array.new
    end

    if !( @quotes.has_key?('data') )
      @log.debug "Your quotes file appears to be corrupt, we'll have to delete it."
      @log.info "Deleting your old quotes file."
      File.delete( @quotes_store )
      @log.info "Creating a new quotes file..."
      File.new( @quotes_store, "w+" )
      @quotes = Hash.new
      @quotes['data'] = Array.new
    end
    rescue; puts $!; end
  end

  def backup
    data = File.open( @quotes_store ) { |f| YAML.load(f) }
    @log.debug "Failed to create the quotes backup file, changes to the main quotes file will " \
               "likely be aborted." and \
    return false unless ( File.open( @quotes_bakup, "w+" ) { |f| YAML.dump( data, f ) } )
    @log.info  "Successfully created a backup of the main quotes file. Chnages can now be " \
               "made safely."
    return true
  end

  def write
    return false unless self.backup
    @log.debug "Failed to write the main quotes file, check your file permissions." and \
    return false unless ( File.open( @quotes_store, "w+" ) { |f| YAML.dump( @quotes, f ) } )
    @log.info  "Wrote the main quotes file."
    return true
  end

  def read
    if !( @quotes = File.open( @quotes_store ) { |f| YAML.load(f) } )
      @log.debug "Error loading the quotes file, creating an empty array."
      @quotes = Hash.new
      @quotes['data'] = Array.new
    end
  end

  def add_quote(m)
    return false if m['quote'].empty?
    @quotes['data'] << "#{Time.now.to_i}:#{m['nick']}:#{m['quote']}"
    @log.info "#{m['nick']} added a quote in #{m['channel']}: #{m['quote']}"
    send_message m['channel'], "Added quote \##{@quotes['data'].size}." if ( self.write )
  end

  def del_quote(m)
    return false unless (m['number'].to_i - 1) <= @quotes['data'].size and \
                        (m['number'].to_i > 0)
    @log.info "#{m['nick']} deleted a quote in #{m['channel']}: #{@quotes['data'][(m['number'].to_i - 1)]}"
    @quotes['data'].delete_at(m['number'].to_i - 1)
    send_message m['channel'], "Deleted quote \##{m['number'].to_i}." if ( self.write )
  end

  def quote(m)
    return false if @quotes['data'].empty?
    num   = rand(@quotes['data'].size)
    quote = @quotes['data'][num].sub(/^(.+?:){2}/,"")
    send_message m['channel'], "[\##{num + 1}] #{quote}"
  end

  def src_quote(m)
    return false if m['search'].empty?
    terms  = m['search'].split.size
    search = /^(.+?:){2}.*#{["(%s).*" % m['search'].gsub('|','\|').split.join('|')]*terms}$/i
    result = Array.new
    @quotes['data'].each_index { |i| result << (i + 1) if @quotes['data'][i] =~ search }
    send_message m['channel'], "Found (#{result.size}) results. #{result.join(', ')}"
  end

  def get_quote(m)
    return false unless (m['number'].to_i - 1) <= @quotes['data'].size and \
                        (m['number'].to_i > 0)
    num   = (m['number'].to_i - 1)
    quote = @quotes['data'][num].sub(/^(.+?:){2}/,"")
    send_message m['channel'], "[\##{num + 1}] #{quote}"
  end

end

plugin = Quotes.new
plugin.add_help 'twist', "quote => A system for recording and replaying clever quips. Available Commands: quote, add, search, del, <number>", true
plugin.add_map "quote add *quote",     "add_quote"
plugin.add_map "quote search *search", "src_quote"
plugin.add_map "quote del #number",    "del_quote"
plugin.add_map "quote #number",        "get_quote"
plugin.add_map "quote",                "quote"
plugin.add_access "add_quote", 25, "r"
plugin.add_access "del_quote", 50, "O"
