class Dice < Plugin

  def initialize; super end

  def do_standard(m)
    roll = rand(6)+1
    msg m['channel'], "Rolled a #{roll}."
    @log.info "#{m['nick']} rolled (#{roll}) in #{m['channel']}."
    return true
  end

  def do_multi(m)
    dice = Array.new
    m['dice'].to_i.times {|x| dice << rand(6) + 1 }
    msg m['channel'], "Rolled: #{dice.join(", ")}."
    @log.info "#{m['nick']} rolled (#{dice.join(', ')}) in #{m['channel']}."
    return true
  end

  def do_crazy(m)
    dice = Array.new
    m['dice'].to_i.times {|x| dice << rand(m['sides'].to_i) + 1 }
    msg m['channel'], "Rolled: #{dice.join(", ")}."
    @log.info "#{m['nick']} rolled (#{dice.join(', ')}) in #{m['channel']}."
    return true
  end

end
plugin = Dice.new
plugin.add_help "dice", "dice => a dice plugin. Available commands: roll, roll <dice>, roll <dice> <sides>, <dice>d<sides>", true
plugin.add_map "roll", "do_standard"
plugin.add_map "roll :dice", "do_multi"
plugin.add_map "roll :dice :sides", "do_crazy"
plugin.add_map "#dice/d/#sides", "do_crazy"
