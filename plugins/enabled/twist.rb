require "yaml"
require "digest/md5"

class Twist < Plugin

  def initialize
    super
    @filedir = "plugins/enabled/twist"
    @filename = "#{@filedir}/medium.yaml"
    @scorefile = "#{@filedir}/score.yaml"
    @twistchan = "#twist"
    @badmasks = ["chatzilla"]
    @twistaddr = nil
    File.open(@filename, "w") if !File.exist?(@filename)
    File.open(@scorefile, "w") if !File.exist?(@scorefile)
    if !(@twistlist = File.open(@filename) {|f| YAML.load(f)})
      @twistlist = Hash.new
      @twistlist['words'] = Hash.new
    end
    @twistlist['temp'] = Hash.new
    @twistlist['words'].each {|word|
      pattern = word.split("").sort.to_s
      if !@twistlist['temp'].keys.include?(pattern)
        @twistlist['temp'][pattern] = Array.new
      end
      @twistlist['temp'][pattern] << word
    }
    @twistlist['words'] = @twistlist['temp']
    @twistlist.delete('temp')
    @twistlist['current'] = Array.new + self.get_random
    @twistlist['jumbled'] = Array.new << self.jumble
#    self.set_topic
    if !(@twistscore = File.open(@scorefile) {|f| YAML.load(f)})
      @twistscore = Hash.new
    end
  end

  def mk_id(addr)
    id = addr.slice(/@.+?$/)
    id = Digest::MD5.hexdigest(id)
    return id if @twistscore.has_key?(id)
    id = addr.slice(/^.+?(?=@)/)
    id = Digest::MD5.hexdigest(id)
    return id unless @badmasks.include?(addr.slice(/^.+?(?=@)/))
    id = addr.slice(/@.+?$/)
    id = Digest::MD5.hexdigest(id)
    return id
  end
  
  def mk_sum(addr)
    sum = 0
    addr.each {|k,v| sum += v }
    return sum
  end
  
  def get_max(addr)
    score = 0
    name = nil
    addr.each {|k,v| 
      name = (v > score)? k : name
      score = (v > score)? v : score
    }
    return name
  end
  
  def check_chan(m)
    if (m['channel'] == @twistchan)
      return true
    else
      if $users[m['nick']].channels.has_key?(@twistchan)
        send_notice m['nick'], "You must be in #{@twistchan} to use that command!"
        return false
      else
        invite m['nick'], @twistchan
        return false
      end
    end
  end

  def twist_save
    File.open(@scorefile, "w") {|f| YAML.dump(@twistscore, f)}
  end
  
  def twist_load(m)
    return false unless (self.check_chan(m))
    if !File.exist?("#{@filedir}/#{m['item']}.yaml")
      send_message @twistchan, "That list does not exist!"
      return false
    end
    @filename = "#{@filedir}/#{m['item']}.yaml"
    if !(loadbuff = File.open(@filename) {|f| YAML.load(f)})
      send_message @twistchan, "Failed to load word list!"
      return false
    elsif !loadbuff.has_key?('words')
      send_message @twistchan, "Invalid word list!"
      return false
    end
    @twistlist['temp'] = Hash.new
    loadbuff['words'].each {|word|
      pattern = word.split("").sort.to_s
      if !@twistlist['temp'].keys.include?(pattern)
        @twistlist['temp'][pattern] = Array.new
      end
      @twistlist['temp'][pattern] << word
    }
    @twistlist['words'] = @twistlist['temp']
    @twistlist.delete('temp')
    @twistlist['current'] = Array.new + self.get_random
    @twistlist['jumbled'] = Array.new << self.jumble
#    self.set_topic
    return true
  end
  
  def listen(m)
    return false unless (m['channel'] == @twistchan)
    if @twistlist['current'].include?(m['message'].split(" ")[0].downcase.delete "?!.,\"'")
      @twistaddr = self.mk_id(m['mask'])
      @twistscore[@twistaddr] = Hash.new unless @twistscore.has_key?(@twistaddr)
      @twistscore[@twistaddr][m['nick']] = 0 unless @twistscore[@twistaddr].keys.include?(m['nick'])
      @twistscore[@twistaddr][m['nick']] += @twistlist['length'] - @twistlist['hints']
      sum = self.mk_sum(@twistscore[@twistaddr])
      send_message @twistchan, "You got it, #{m['nick']}\(#{sum}\)!Possible words: #{@twistlist['current'].join(", ")}"
      @twistlist['current'] = Array.new + self.get_random
      @twistlist['jumbled'] = Array.new << self.jumble
      send_message @twistchan, "The new word is: #{@twistlist['jumbled'].to_s}"
#      self.set_topic
      self.twist_save
    end
  end

  def on_join(m)
    send_notice m['nick'], "The current word is: #{@twistlist['jumbled'].to_s}" if (m['channel'] == @twistchan)
    return true
  end

  def twist_new(m)
    return false unless (self.check_chan(m))
    if @twistlist['words'].length == 0
      send_message @twistchan, "No words in word list!"
      return false
    end
    @twistlist['current'] = Array.new + self.get_random
    @twistlist['jumbled'] = Array.new << self.jumble
    send_message @twistchan, "New word: #{@twistlist['jumbled'].to_s}"
#    self.set_topic
    return true
  end

  def twist(m)
    return false unless (self.check_chan(m))
    if @twistlist['words'].length == 0
      send_message @twistchan, "No words in word list!"
      return false
    end
    send_message @twistchan, "The current word is: #{@twistlist['jumbled'].to_s}"
    return true
  end

  def twist_hint(m)
    return false unless (self.check_chan(m))
    if @twistlist['current'].length == 0
      send_message @twistchan, "Hint for what!?"
      return false
    end
    hints = Array.new
    @twistlist['current'].each {|x| 
      hints << x.to_s[0..@twistlist['hints']].upcase
    }
    if (@twistlist['hints'] < @twistlist['length'])
      @twistlist['hints'] += 1
    end
    send_message @twistchan, "Hint: #{hints.uniq.join(", ")}"
    return true
  end
  
  def twist_score(m)
    return false unless (self.check_chan(m))
    @twistaddr = self.mk_id(m['mask'])
    if !@twistscore.has_key?(@twistaddr)
      send_message @twistchan, "You don't have a score!"
      return false
    end
    sum = self.mk_sum(@twistscore[@twistaddr])
    send_message @twistchan, "Your score is: #{sum}"
    return true
  end
  
  def twist_board(m)
    holder = Hash.new
    @twistscore.keys.each {|player|
       sum = self.mk_sum(@twistscore[player])
       if !holder.has_key?(sum)
         holder[sum] = Array.new
       end
       holder[sum] << self.get_max(@twistscore[player])
    }
    holder = holder.to_a.sort
    out = Array.new
    holder.size.times {|x| out << "#{x+1}: #{holder.pop.join(", ")}" }
    send_message @twistchan, out.join(" | ")
  end
    
  def get_random
    @twistlist['hints'] = 0
    pattern = @twistlist['words'].keys.shuffle.pop
    @twistlist['length'] = pattern.length
    return @twistlist['words'][pattern]
  end
  
  def jumble
    return @twistlist['current'][0].split("").shuffle.to_s.upcase
  end
  
#  def set_topic
#    @bot.say "ChanServ", "TOPIC #{@twistchan} The current word is: #{@twistlist['jumbled'].to_s}"
#    return true
#  end

  def twist_view(m)
    return false unless (self.check_chan(m))
    send_message @twistchan, "Current word list: #{@filename}."
    send_message @twistchan, "Number of words: #{@twistlist['words'].length}."
  end

end

plugin = Twist.new
plugin.add_map 'twist wordlist :item', 'twist_load'
plugin.add_map 'twist load :item', 'twist_load'
plugin.add_map 'twist new', 'twist_new'
plugin.add_map 'twist next', 'twist_new'
plugin.add_map 'twist n', 'twist_new'
plugin.add_map 'twist hint', 'twist_hint'
plugin.add_map 'twist h', 'twist_hint'
plugin.add_map 'hint', 'twist_hint'
plugin.add_map 'h', 'twist_hint'
plugin.add_map 'twist score', 'twist_score'
plugin.add_map 'twist scores', 'twist_board'
plugin.add_map 'twist scoreboard', 'twist_board'
plugin.add_map 'twist board', 'twist_board'
plugin.add_map 'twist stats', 'twist_view'
plugin.add_map 'twist info', 'twist_view'
plugin.add_map 'twist', 'twist'

plugin.add_help 'twist', "twist => A word twist plugin playable in #twist. Available Commands: wordlist, load, new, hint, score, scores, view, stats.", true
plugin.add_help 'twist wordlist', "twist wordlist => Changes wordlist. Ask the bot operator for a list of these.", false
plugin.add_help 'twist new/next', "twist new/twist next => Goes to the next word.", false
plugin.add_help 'twist hint', "twist hint => Gives the next letter as a hint.", false
plugin.add_help 'twist score', "twist score => Shows your score.", false
plugin.add_help 'twist stats', "twist stats => Shows some stats about the wordlist.", false