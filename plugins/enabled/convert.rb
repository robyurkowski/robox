class Convert < Plugin

  def initialize; super; end
  
  def to_fahrenheit(m)
    c_temp = (((m['temp'].to_f * 9.0) / 5.0) + 32).round
    msg m['channel'], "#{m['temp'].to_s}ºC: #{c_temp.to_s}ºF"
    return true
  end
  
  def to_celsius(m)
    c_temp = (((m['temp'].to_f - 32) / 9.0) * 5.0).round
    msg m['channel'], "#{m['temp'].to_s}ºF: #{c_temp.to_s}ºC"
    return true
  end
  
  def to_lbs(m)
    c_weight = (m['weight'].to_f * 2.2).round
    msg m['channel'], "#{m['weight'].to_s} kg: #{c_weight.to_s} lbs"
    return true
  end
  
  def to_kg(m)
    c_weight = (m['weight'].to_f / 2.2).round
    msg m['channel'], "#{m['weight'].to_s} lbs: #{c_weight.to_s} kg"
    return true
  end
end

plugin = Convert.new
plugin.add_help "convert", "convert => plugin to convert between measurements. see 'help convert units' for a list.", true
plugin.add_help "convert units", "convert units => with form 'convert <number><unit> to <units>': f, c, lbs, kg", false
plugin.add_map "convert :temp/c to f", "to_fahrenheit"
plugin.add_map "convert :temp/f to c", "to_celsius"
plugin.add_map "convert :weight to kg", "to_kg"
plugin.add_map "convert :weight to lbs", "to_lbs"