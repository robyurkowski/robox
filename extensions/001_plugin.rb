#
# PLUGINS CORE EXTENSION
# VERSION 1.0 - 7/16/2009 - Jason Van Dyke
# + First functional release.
# VERSION 1.1 - 7/18/2009 - Jason Van Dyke
# + Added minor functionallity.
# + Updated create_message_data to reflect changes in handle_message data types.
# + Added "raw" as a reserved hash key.
# VERSION 1.2 - 7/19/2009 - Jason Van Dyke
# + Added on_join, on_part, on_nick event methods
# + Added core command to load/reload plugins
# + Added core command to unload single plugins
# + Changed plugin logging.
#   - Plugins automatically log their actions if their 'initialize' method calls super.
#   - Plugins can access that log through '@log' to record events and such.
# VERSION 1.3 - 7/20/2009 - Jason Van Dyke
# + Added '#key' match type to 'add_map' (matches [\d]+? *a string of numbers)
# + Can now use '/' as a delimiter in 'add_map'
#     ie: ":who/b/:what" => "<who>b<what>"
#     (or if you know regex) => /^([^\s]+?)b([^\s]+?)$/
#   * NOTE : This means you CANNOT use "/" in your commands, sorry.
#
# This extension adds plugin support to robox.
#
# Instructions for installation:
# - Add this file to your "{robox root directory}/extensions/" sub-directory
# - Add any plugins to the "{robox root directory}/plugins/" sub-directory (if it does not exist, create it)
#
# Instructions for creating plugins:
# - Plugins are capable of two types of interaction; to use either, you must follow these basic steps.
# - * All plugin methods must be created within a subclass of the Plugin class...
#     |  class Greeter < Plugin
#     |    def say_welcome(m) ...
# - * Any plugin method which recieves data from the server must have a single parameter. The information
#     which is sent to the plugin is as follows:
#       HASH {
#         :nick => the nick of the message's source (string)
#         :mask => the username/hostmask of the message's source (string)
#         :channel => the location in which the message was recieved (string)
#         :message => the contents of the message (string)
#       }
# - * Any plugin which requires data to be streamed from the server requires a "listen" method with a
#     single parameter (through which it recieves the message data hash). However, this method of interaction
#     can easily become computationally expensive and should only be used sparingly.
# - * To add core-command-like functionality to a plugin, the method which is recieving server data must
#     be "mapped" similarly to this:
#     |  plugin = Greeter.new
#     |  plugin.add_map "hello", "say_welcome"
#     In this way, when the command "hello" is used, server data will be sent to the "say_welcome" method
#     of this plugin. Additionally, more data can be sent to the plugin by adding parameters to your command map
#     like so:
#     |  plugin = Greeter.new
#     |  plugin.add_map "greet :name", "say_welcome"
#     or:
#     |  plugin.add_map "greet *names", "say_welcome"
#     Using the key-character ":" in a plugin map followed by any combination of letters (except those which are
#     reserved), will match the word occuring there in the server message and place it in the message data hash
#     under the key you defined:
#       MESSAGE DATA HASH {
#         :nick => Jason, 
#         :mask => Jason@The.Web.Guru,
#         :channel => #robox,
#         :message => ~greet Rob,
#         :name => Rob
#       }
#     Similarly, you can use the character "*" followed by your desired hash key to match a longer string of words:
#       MESSAGE DATA HASH {
#         ...
#         :message => ~greet Rob, Becky, Mary, Adam
#         :names => Rob, Becky, Mary, Adam
#       }
#     You can also define multiple parameters when mapping your plugin functions:
#     |  plugin = Greeter.new
#     |  plugin.add_map "greet :greeting to *names", "say_welcome"
#       MESSAGE DATA HASH {
#         ...
#         :message => ~greet Welcome to Rob, Becky, Mary, Adam
#         :greeting => Welcome
#         :names => Rob, Becky, Mary, Adam
#       }
#     The hash keys which are reserved are the standard ones sent in the message data hash (nick,mask,channel,message)
# - * An example of a simple greeting plugin might look like this:
#
# | class Greeter < Plugin
# |   def say_welcome(m)
# |     send_message m['channel'], "#{m['greeting']} #{m['names']}, my name is Robox!"
# |   end
# | end
# | plugin = Greeter.new
# | plugin.add_map "greet :greeting to *names", "say_welcome"
#

$language.push "@plugin_loading", "Loading plugin: %s ..."
$language.push "@plugin_loading_failed", "Failed to load plugin: %s"
$language.push "@plugin_unload", "Unloading plugin: \"%s\""
$language.push "@plugin_unload_error", "Plugin \"%s\" not found; aborting."
$language.push "@plugin_unload_success", "Plugin \"%s\" unloaded."
$language.push "@plugin_no_method", "Error! Method '%s' does not exist."
$language.push "@plugin_reserved_key", "Error! Cannot use reserved hash key '%s' in '%s'."
$language.push "@plugin_add_map", "Now matching: %s, With hash keys: %s"
$language.push "@plugin_add_access", "Setting access for '%s'. access:'%s' flags:'%s'"
$language.push "@plugin_initialize", "Initializing %s Plugin at location: %s ..."

plugins = CoreCommand.new "list plugins"
plugins.help   = "list plugins => lists the active plugins."
plugins.active = true
plugins.regex  = /^list plugins$/i
plugins.access = {"access" => 75, "flags" => "Or"}
plugins.block  = Proc.new { |core,m,r| core.send_message m['channel'], core.plugins.collect {|c| c.class.to_s + "  "} }

plugins = CoreCommand.new "load plugins"
plugins.help   = "(re)load plugins => reloads all of your plugins."
plugins.active = true
plugins.regex  = /^(re)?load plugins$/i
plugins.access = {"access" => 75, "flags" => "Or"}
plugins.block  = Proc.new { |core,m,r| core.send_message m['channel'], core.load_plugins }

plugins = CoreCommand.new "unload plugin"
plugins.help   = "unload plugin [plugin] => unloads [plugin]"
plugins.active = true
plugins.regex  = /^unload plugin (.+?)$/i
plugins.access = {"access" => 75, "flags" => "Or"}
plugins.block  = Proc.new { |core,m,r| core.send_message m['channel'], core.unload_plugin(r[1]) }

class IRCClient

  def    plugins;     @@plugin;     end
  def    plugins_map; @@plugin_map; end
  alias :plugin       :plugins
  alias :plugin_map   :plugins_map

  # connect(nil)
  # - Extends the connection method to include plugin loading.
  alias :plugin_connect :connect
  def connect()
    # CALL ORIGINAL FUNCTION
    plugin_connect
    # PLUGIN EXTENSION
    load_plugins
    return true
  end

  # handle_server_input(string)
  # - Extends the handle_server_input method so that data may be streamed to plugins.
  alias :plugin_handle_server_input :handle_server_input
  def handle_server_input(s)
    # CALL ORIGINAL FUNCTION
    plugin_handle_server_input(s)
    # PLUGIN EXTENSION
    # - Streams server messages to plugins which include a "listen" method.
    # TODO: Modify IRCClient.Users to catch login proceedures.
    s = s.strip
    case s
      when /^:?(.+?)!(.+?) (PRIVMSG|NOTICE) (.+?) :?(.+?)$/i #LISTEN
        m = Hash["nick", $1, "mask", $2, "channel", $4, "message", $5, "raw", s]
        m['channel'] = ( m['channel'] == @@state['nickname'] ) ? m['nick'] : m['channel']
        @@plugin.each {|p| p.__send__ :listen, m if p.respond_to?("listen") }
        
      when /^:?(.+?)!(.+?) JOIN :?(.+?)$/i # ON JOIN
        m = Hash[ "nick", $1, "mask", $2, "channel", $3, "raw", s ]
        @@plugin.each {|p| p.__send__ :on_join, m if p.respond_to?("on_join") }

      when /^:?(.+?)!(.+?) PART (.+?) ?:?.*$/i # ON PART
        m = Hash[ "nick", $1, "mask", $2, "channel", $3, "raw", s ]
        @@plugin.each {|p| p.__send__ :on_part, m if p.respond_to?("on_part") }

      when /^:?(.+?)!(.+?) NICK :?(.+?)$/i # ON NICK
        m = Hash[ "nick", $1, "mask", $2, "newnick", $3, "raw", s ]
        @@plugin.each {|p| p.__send__ :on_nick, m if p.respond_to?("on_nick") }

      else
        return false
    end
  end
  private :handle_server_input

  # handle_message(string,string,string)
  # - Extends the core command handler to send data to plugin-defined methods.
  alias :plugin_handle_message :handle_message
  def handle_message(m)
    # CALL ORIGINAL FUNCTION
    return true if plugin_handle_message(m) # Core command has been called
    # PLUGIN EXTENSION
    # TODO: fix regex matching so that commands with invalid input return errors
    # - Allows plugins to emulate core-command behavior by calling them using the command prefix. The server message
    #   is cycled through each registed plugin; when a match is found it sends the message to that plugin and its
    #   defined function.
    command = m['message'].sub(@@state['responder'],"")
    @@plugin_map.each {|c|
      if c[0][0].match(command).to_s == command
        access = (c[1].access[c[2]].nil?) ? c[1].access['global'] : c[1].access[c[2]]
        return true unless check_access(@@users[m['nick']],access)
        c[1].__send__ c[2].intern, create_message_data(m,c[0])
      end
    }
  end
  private :handle_message

  # create_message_data(string,array|nil)
  # - PARAMETERS:
  # - - s => server message ":Jason!Jason@The.Web.Guru #robox :Haha, is this a good example?"
  # - - reg => for mapped commands, reg is the regular expression data. [["command (.+?) is a (.*)"],["name","thing"]]
  # - DESCRIPTION:
  # - - Maps matched groups from a server message to the message data hash.
  # - - If the data is intended for a registered command, the message data is run through another regexp which
  #     adds plugin-defined parameters to the message data before it is to be sent.
  def create_message_data(message_data,reg=nil)
    command = message_data['message'].sub(@@state['responder'],"")
    if reg != nil and reg[1].size > 0
      parameter_match = reg[0].match(command).to_a.reverse
      parameter_match.pop #remove the first match from the match data (matches the entire message)
      reg[1].each {|k| message_data[k] = parameter_match.pop.strip } #for each parameter, add match data to the message hash
    end
    return message_data
  end
  private :create_message_data

  # load_plugins(nil)
  # Load each plugin from the plugins directory.
  # *NOTE: Need to create more functionality for failed plugins.
  def load_plugins
    @@plugin = Array.new #array of plugin object references
    @@plugin_map = Array.new #array holding plugin mapping data
    @@plugin_failed = Array.new #array holding the physical address' of failed plugins
    Dir.glob('plugins/enabled/*.rb').each do |f|
      begin
        load f
        @log.debug $language.plugin_loading % f
      rescue
        @@plugin_failed << f
        @log.debug $language.plugin_loading_failed % f
      end
    end
    if !@@plugin_failed.empty?
      return $language.error % @@plugin_failed.join(",") + " could not be loaded."
    else
      return $language.okay
    end
  end

  # unload_plugin("Plugin Class Name")
  # Removes all references to the plugin object so that it can be destroyed.
  # TODO: Unmap all help, as well.
  def unload_plugin(plugin)
    @log.debug $language.plugin_unload % plugin
    unload_me = nil
    @@plugin.each {|p| if p.class.to_s.downcase == plugin.downcase; unload_me = p; break; end }

    if unload_me == nil
      fail = $language.plugin_unload_error % plugin
      @log.debug fail
      return fail
    end
    success = $language.plugin_unload_success % plugin
    @log.debug success
    @@plugin.delete unload_me
    
    @@plugin_map.each_index {|i| @@plugin_map[i] = nil if @@plugin_map[i][1] == unload_me}
    @@plugin_map.compact!
    
    @@commands.each {|k, c| @@commands.delete k if c.plugin == unload_me.class}
        
    return success
  end

end

# Adds hooks for user login and logout methods.
class User < IRCClient

  alias :plugin_log_in :log_in
  def log_in(password)
    plugin_log_in(password)
    @@plugin.each {|p| p.__send__ :on_login, self if p.respond_to?("on_login") }
    return true
  end

  alias :plugin_log_out :log_out
  def log_out
    plugin_log_out
    @@plugin.each {|p| p.__send__ :on_logout, self if p.respond_to?("on_logout") }
    return true
  end

end

# The plugin class
class Plugin < IRCClient

  attr_reader :access, :log

  def inspect; 
    " {self} [ #{self.instance_variables.join(', ')} " << ( self.methods - 
      self.class.superclass.superclass.methods ).join(', ') << " ]"
  end

  def initialize
    # ACCESS BLOCK
    @access = Hash.new
    @access['global'] = { "access", 0, "flags", "" }
    # LOG BLOCK
    @log = Log.new("#{@@state['loggerlocation']}#{self.class.superclass.to_s.downcase}.#{self.class.to_s.downcase}.log")
    @log.debug $language.plugin_initialize % [self.class,self]
    @@plugin << self unless @@plugin.include?(self)
  end
  
  # add_map(string,string)
  # - PARAMETERS:
  # - - regexp_string => pseudo regexp string "command :single_param, *multi_param"
  # - - function_name => method the plugin class will call when a regex match is found
  # - DESCRIPTION:
  # - - Checks the plugin class for the method to be mapped.
  # - - Checks the pseudo regex for reserved hash keys *NOTE: need to check for special characters also (or maybe
  #     regex quote)
  # - - If there are no errors it creates a real regular expression with groupings and an array of keys:
  # - -   "command :single_param, *multi_param" => "command (.+?), (.*)"
  # - -   keys = ['single_param','multi_param']
  # - - Then it stores the regexp info and the object/method references in the @@plugin_map variable.
  def add_map(regexp_string,function_name)
    if !self.respond_to?(function_name)
      @log.debug $language.plugin_no_method % function_name
      return false
    elsif regexp_string =~ /[#:\*](newnick|nick|mask|channel|message|raw)/i
      @log.debug $language.plugin_reserved_key % [$1,regexp_string]
      return false
    end
    parameter_map = regexp_string.scan( /[#:\*](\w+)/ ).to_a.flatten
    regexp_string = regexp_string.gsub( /#\w+/  , '(\d+?)'    )
    regexp_string = regexp_string.gsub( /:\w+/  , '([^\s]+?)' )
    regexp_string = regexp_string.gsub( /\*\w+/ , '(.*)'      )
    regexp_string = regexp_string.gsub( /\//    , ''          )
    regexp_string = /^#{regexp_string}$/
    @@plugin_map << [ [ regexp_string, parameter_map ], self, function_name ]
    @log.info $language.plugin_add_map % [regexp_string.to_s,parameter_map.join(', ')]
    return true
  end
  alias :set_map :add_map

  def add_access( method, access = 0, flags = "" )
    return false unless self.respond_to?(method)
    @access.merge!( { method => { "access" => access, "flags" => flags } } )
	@log.info $language.plugin_add_access % [method,access,flags]
    return true
  end
  alias :set_access :add_access

  def add_help( nomen, help, list = false )
    help = help.to_a
    @@commands.each_value { |c| return false if c.name == nomen }
    command = CoreCommand.new nomen
    command.active = false
    command.help   = help
    command.list   = list
    command.plugin = self.class
    return true
  end

end
