class CoreCommand < IRCClient

  attr_accessor :help, :regex, :access, :block, :active, :list, :plugin

  attr_reader :name

  def initialize(nomen)
    @active = true
    @list   = true
    @name   = nomen
    @@commands ||= {}
    @@commands[@name.to_s] = self
  end
  
  def name=(nomen)
    old_name = @name
    @name = nomen
    @@commands[@name] = self
    @@commands.delete old_name
  end

end

# NOTE: MAKE EACH COMMAND A Proc.new block with 3 args!
# core -> self from IRCClient class
# m -> m from handle_message
# r -> regex match data

# CoreCommand.new params:
# @regex  = /Regex/
# @access = { :access => 50, :flags => "Abc" }
# @block  = Proc.new { |core,m,r| CODE BLOCK }
# @active = true/false (assumes true)

help = CoreCommand.new "help"
help.help   = "help [topic] => displays help information for [topic]"
help.regex  = /^help (.+?)$/
help.access = {"access" => 0, "flags" => ""}
help.block  = Proc.new do |core,m,r|
  # com contains an array of matching commands; empty if no matches
  matches = core.commands.keys.grep(/^#{r[1..-1].join("")}$/i)
  if (!matches.empty?)
    com = core.commands[matches[0]]
    if (!com.help.nil?)
      com.help.to_a.each { |help| core.send_message m['channel'], help }
    else
      core.send_message m['channel'], $language.core_no_help % matches[0]
    end
  else
    core.send_message m['channel'], $language.core_no_such_command
  end
end

help = CoreCommand.new "commands"
help.list   = false
help.regex  = /^help$/
help.access = {"access" => 0,"flags" => ""}
help.block  = Proc.new do |core,m,r|
  topics = Array.new
  core.commands.each_value { |c| topics << c.name unless c.list == false }
  core.send_message m['channel'], "Topics: #{topics.sort.join(', ')}"
end

eval = CoreCommand.new "eval"
eval.help   = "eval => evaluates a single line of ruby code."
eval.regex  = /^eval\s/i
eval.active = true
eval.access = {"access",100,"flags","Ar"}
eval.block  = Proc.new do |core,m,r|
  m['message'].sub!(/^.+?eval\s/i,"")
  core.send_message m['channel'], eval($')
end

uptime = CoreCommand.new "uptime"
uptime.help   = "uptime => displays the total time the bot has been running."
uptime.regex  = /^uptime$/i
uptime.access = {"access" => 25,"flags" => "r"}
uptime.block  = Proc.new do |core,m,r|
  s = (Time.now - core.state['starttime']).to_i
  n = (s >= 60 )? (s / 60).to_i : 0
  s = (s >= 60 )? (s % 60)      : s
  h = (n >= 60 )? (n / 60).to_i : 0
  n = (n >= 60 )? (n % 60)      : n
  d = (h >= 24 )? (h / 24).to_i : 0
  h = (h >= 24 )? (h % 24)      : h
  core.send_message m['channel'], $language.core_uptime % [d,h,n,s]
end

say = CoreCommand.new "say"
say.help   =  "say [target] [*message] => sends [*message] to [target]"
say.regex  = /^say (.+?) (.+?)$/i
say.access = {"access" => 25, "flags" => "r"}
say.block  = Proc.new { |core,m,r| core.send_message(r[1],r[2]) }

act = CoreCommand.new "act"
act.help   = "act [target] [*action] => sends [*action] to [target]"
act.regex  = /^act (.+?) (.+?)$/i
act.access = {"access" => 25, "flags" => "r"}
act.block  = Proc.new { |core,m,r| core.send_action(r[1],r[2]) }

notice = CoreCommand.new "notice"
notice.help   = "notice [target] [*message] => sends [*message] to [target]"
notice.regex  = /^notice (.+?) (.+?)$/i
notice.access = {"access" => 25, "flags" => "r"}
notice.block  = Proc.new { |core,m,r| core.send_notice(r[1],r[2]) }

kick = CoreCommand.new "kick"
kick.help   = "kick [channel] [target] (reason) => kicks [target] from [channel]"
kick.regex  = /^kick (.+?) ([^\s]*) ?(.+?)?$/i
kick.access = {"access" => 50, "flags" => "Or"}
kick.block = Proc.new do |core,m,r|
  reason = (defined? r[3]) ? r[3] : nil
  core.kick(r[2],r[1],reason)
end

nick = CoreCommand.new "nick"
nick.help   = "nick [name] => changes the bot's nick to [name]"
nick.regex  = /^nick (.+?)$/i
nick.access = {"access" => 75, "flags" => "Ar"}
nick.block  = Proc.new { |core,m,r| core.set_nickname(r[1]) }

topic = CoreCommand.new "topic"
topic.help   = "topic [channel] [topic] => sets the topic in [channel] to [topic]"
topic.regex  = /^topic (.+?) (.+?)$/i
topic.access = {"access" => 50, "flags" => "Or"}
topic.block  = Proc.new { |core,m,r| core.set_topic(r[1],r[2]) }

join = CoreCommand.new "join"
join.help   = "join [channel] => makes the bot join [channel]"
join.regex  = /^join (.+?)$/i
join.access = {"access" => 50, "flags" =>"Or"}
join.block  = Proc.new { |core,m,r| core.join_channel(r[1]) }

part = CoreCommand.new "part"
part.help   = "part [channel] => makes the bot leave [channel]"
part.regex  = /^part ([^\s]*) ?(.+?)?$/i
part.access = {"access" => 50, "flags" => "Or"}
part.block  = Proc.new { |core,m,r| core.part_channel(r[1]) }

quit = CoreCommand.new "quit"
quit.help   = "quit => makes the bot exit."
quit.regex  = /^quit ?(.+?)?$/i
quit.access = {"access" => 75, "flags" => "Ar"}
quit.block  = Proc.new do |core,m,r|
  reason = (defined? r[1]) ? r[1] : @@state['defaultquit']
  core.disconnect(r[1])
end

mode = CoreCommand.new "mode"
mode.help   = "mode [mode] (channel) (target) => sets [mode] with optional (channel) and (target)"
mode.regex  = /^mode ([^\s]+?)$/i
mode.access = {"access" => 75, "flags" => "Ar"}
mode.block  = Proc.new { |core,m,r| core.set_mode(r[1],nil,nil) }

mode = CoreCommand.new "mode channel"
mode.list   = false
mode.regex  = /^mode ([^\s]+?) ([^\s]+?)$/i
mode.access = {"access" => 50, "flags" => "Or"}
mode.block  = Proc.new { |core,m,r| core.set_mode(r[1],r[2],nil) }

mode = CoreCommand.new "mode channel user"
mode.list   = false
mode.regex  = /^mode ([^\s]+?) ([^\s]+?) (.+?)$/i
mode.access = {"access" => 50, "flags" => "Or"}
mode.block  = Proc.new { |core,m,r| core.set_mode(r[1],r[2],r[3]) }

invite = CoreCommand.new "invite"
invite.help   = "invite [user] [channel] => invites [user] to [channel]"
invite.regex  = /^invite (.+?) (.+?)$/i
invite.access = {"access" => 25, "flags" => "r"}
invite.block  = Proc.new { |core,m,r| core.invite(r[1],r[2]) }

# USER COMMANDS

login = CoreCommand.new "login"
login.help   = "login [password] => registers your session with the bot."
login.regex  = /^login (.+?)$/i
login.access = {"access" => 0, "flags" => ""}
login.block  = Proc.new { |core,m,r|
  core.users[m['nick']].log_in r[1]
  core.send_message(m['channel'],$language.user_login_success) }

logout = CoreCommand.new "logout"
logout.help   = "logout => unregisters your session with the bot."
logout.regex  = /^logout$/i
logout.access = {"access" => 0, "flags" => ""}
logout.block  = Proc.new { |core,m,r|
  core.users[m['nick']].log_out
  core.send_message(m['channel'],$language.user_logout_success) }

register = CoreCommand.new "register"
register.help   = [ "register [password] => registers your current nickname with the bot.",
                    "register alias [name] => registers an alias, [name], to your account." ]
register.regex  = /^register alias.*$/i
register.access = {"access" => 0, "flags" => ""}
register.block  = Proc.new { |core,m,r|
  core.users[m['nick']].user_register_alias
  core.send_message(m['channel'],$language.user_alias_success % m['nick']) }

register = CoreCommand.new "register user"
register.list   = false
register.regex  = /^register (.+?)$/i
register.access = {"access" => 0, "flags" => ""}
register.block  = Proc.new { |core,m,r|
  core.users[m['nick']].user_register r[1]
  core.send_message(m['channel'],$language.user_register_success % m['nick']) }

drop = CoreCommand.new "drop"
drop.help   = [ "drop => deletes all records of your account from the bot.",
                "drop [alias] => removes an alias, [alias], from your account." ]
drop.regex  = /^drop alias ?(.+?)?$/i
drop.access = {"access" => 0, "flags" => ""}
drop.block  = Proc.new { |core,m,r|
  name = (defined? r[1]) ? r[1] : m['nick']
  core.users[m['nick']].user_drop_alias name
  core.send_message(m['channel'],$language.user_drop_alias_success % name) }

drop = CoreCommand.new "drop name"
drop.list   = false
drop.regex  = /^drop$/i
drop.access = {"access" => 0, "flags" => ""}
drop.block  = Proc.new { |core,m,r|
  core.users[m['nick']].user_drop
  core.send_message(m['channel'],$language.user_drop_success % m['nick']) }
