# Used to generate the config.yml file in the root directory.
require 'yaml'

def yn
  answer = gets.chomp.upcase.to_a
  if (answer[0] == 'Y')
    true
  elsif (answer[0] == 'N')
    false
  else
    puts "Improper entry. Y/N?"
    return yn
  end
end

def generate_yaml
  $new_config = {}
  puts "Robox Config Generator"
  puts "----------------------"
  puts "Server you'd like to connect to? (eg. 'irc.quakenet.org'):  "
  $new_config['server'] = gets.chomp.to_s
  puts "Server port to connect to? (eg. '6667'):  "
  $new_config['port'] = gets.chomp.to_s
  puts "What's the bot's nickname? (eg. 'Robox'):  "
  $new_config['nickname'] = gets.chomp.to_s
  puts "What should trigger bot response? (eg. 'Robox' or '='):  "
  $new_config['responder'] = gets.chomp.to_s
  puts "Which channels should the bot join automatically?"
  puts "Seperate multiples with commas, and use a space after"
  puts "the name to denote a channel key. (eg. '#chat,#help bananas'):  "
  $new_config['channels'] = gets.chomp.split(%r{,\s*})
  puts "What should the bot's quit message be by default? (eg. 'So long!'):  "
  $new_config['defaultquit'] = gets.chomp.to_s
  puts "Where should the bot leave its system logs? (eg. '/opt/robox/logs/system/system.log')"
  puts "Use either a path relative to the installation directory (eg."
  puts "'logs/system/system.log' or an absolute path, but don't use"
  puts "paths that would need to be expanded (eg. ~/robox)"
  $new_config['loggerlocation'] = gets.chomp.to_s
  Kernel.system('clear')
  puts $new_config.to_yaml
  puts ""
  puts "Does this look all right? (Y/N): "
  if yn
    return true
  else
    return generate_yaml
  end
end

file = File.join(File.dirname(__FILE__), '/config.yml')

Kernel.system('clear')
if generate_yaml
  begin
    open(file, 'w') {|f| YAML.dump($new_config, f)}
  rescue Errno::ENOENT
    Kernel.system("touch #{file}")
    retry
  end
end

Kernel.system("mv config.yml ../")



