require 'rubygems'
require 'sqlite3'

$db = "robox.db"
populate = %w(config users state)


class SQLTable

  # @fields
  # @db
  # @name

  # fields: "field" => "VARCHAR(255)"
  def initialize(name, fields, db = $db, &block)
    @name = name
    @db = SQLite3::Database.new(db)
    @fields = fields
    
    arr = []
    fields.each { |k, c| arr.push "#{k} #{c}" }
    str = arr.join(", ")
    
    @db.execute("CREATE TABLE IF NOT EXISTS #{name} (#{str});")
    
    self.instance_eval(&block) if block_given?
  end

  def insert(values)
    v = values.collect {|p| "'" + p + "'"}.join(", ")
    query = "INSERT INTO #{@name} (#{@fields.keys.join(", ")}) VALUES (#{v});"    
    @db.execute query
  end
end

# config = SQLTable.new('config', {"field" => "VARCHAR(255)", "value" => "VARCHAR(255)"}) do
#   insert(%w(version Robox))
#   insert(%w(access_mode flags))
#   insert(%w(user_default_access 25))
#   insert(%w(user_default_flags r))
#   insert(%w(user_register users/register.yml))
# end

# state_fields = {"field" => "VARCHAR(255)", "value" => "VARCHAR(255)"}
# state = SQLTable.new('state', state_fields) do
#   insert(%w())
# end

users_fields = {}
users_fields['nickname']      = "VARCHAR(255)"
users_fields['username']      = "VARCHAR(255)"
users_fields['hostname']      = "VARCHAR(255)"
users_fields['flags']         = "VARCHAR(255)"
users_fields['attributes']    = 




users = SQLTable.new('users', )
--- 
Rob: 
  flags: AOr
  nickname: Rob
  attributes: {}

  time: 0
  access: 100
  password: admin



# db.execute("CREATE TABLE channels (field VARCHAR(255), value VARCHAR(255));")
# db.execute("CREATE TABLE state (field VARCHAR(255), value VARCHAR(255));")
# db.execute("CREATE TABLE users (field VARCHAR(255), value VARCHAR(255));")
# 
# 
# db.execute("CREATE TABLE channel_logs (field VARCHAR(255), value VARCHAR(255));")
# db.execute("CREATE TABLE system_logs (field VARCHAR(255), value VARCHAR(255));")
# db.execute("CREATE TABLE plugin_logs (field VARCHAR(255), value VARCHAR(255));")
# db.execute("CREATE TABLE user_logs (field VARCHAR(255), value VARCHAR(255));")
